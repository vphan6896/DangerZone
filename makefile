BLACK         := black
CHECKTESTDATA := checktestdata
COVERAGE      := coverage3
MYPY          := mypy
PYDOC         := pydoc3
PYLINT        := pylint
PYTHON        := python3
PIP			  := pip

upgrade:
	$(PIP) install --upgrade pip

test-api:
	(cd test/api && docker-compose up --build)

test-acceptance:
	(cd test/acceptance && docker-compose up --build)

zip-all:
	(cd src/backend && zip -r backend-src.zip * && mv backend-src.zip ../../)
	(cd src/frontend && zip -r frontend-src.zip * && mv frontend-src.zip ../../)

frontend:
	docker build -t frontend:latest src/frontend
	docker run -d -p 127.0.0.1:3000:3000 frontend

backend:
	(cd src/backend && docker-compose up --build -d)

backend-prod:
	docker build -t backend-prod:latest src/backend/local_api
	docker run --env-file=src/backend/local_api/.env -d -p 127.0.0.1:8080:8080 backend-prod


all:

clean: