from flask import Flask, jsonify
from sqlalchemy import create_engine
from sqlalchemy import Table, Column, String, MetaData
import os

app = Flask(__name__)
db_string = str.format(
    "postgresql://{}:{}@{}/{}",
    os.environ["DB_USERNAME"],
    os.environ["DB_PASSWORD"],
    os.environ["DB_HOST"] + ":" + os.environ["DB_PORT"],
    os.environ["DB_DATABASE"],
)
db = create_engine(db_string)
metadata = MetaData(db)
# Create a table with the appropriate Columns
test_table = Table("test", metadata, Column("name", String))
# Implement the creation
metadata.create_all()


@app.route("/select", methods=["GET"])
def select():
    result = ""
    with db.connect() as conn:
        select_statement = test_table.select()
        result_set = conn.execute(select_statement)
        for r in result_set:
            result += r[0] + "\n"
    return result


@app.route("/insert", methods=["POST"])
def insert():
    with db.connect() as conn:
        insert_statement = test_table.insert().values(name="sup")
        conn.execute(insert_statement)
    return "success"


@app.route("/health", methods=["GET"])
def health():
    return "Backend is healthy"


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)
