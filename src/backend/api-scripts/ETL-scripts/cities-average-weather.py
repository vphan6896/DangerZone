import json


def main():
    with open('../assets/cities/cities_average_weather.json', 'r') as cities_average_weather_file:
        cities_average_weather_data = cities_average_weather_file.read()

    with open('../assets/cities.json', 'r') as cities_file:
        cities_data = cities_file.read()

    # parse file
    cities = json.loads(cities_data)
    cities_average_weather = json.loads(cities_average_weather_data)

    for city in cities:
        city['average_high'] = 0
        city['average_low'] = 0
        for city_weather in cities_average_weather:
            if city_weather['city'] == city['city']:
                city['average_high'] = city_weather['average_high']
                city['average_low'] = city_weather['average_low']
                break

    # print(crime_json)
    file = open("cities.json", "w")
    file.write(json.dumps(cities))
    print(json.dumps(cities))


if __name__ == '__main__':
    main()
