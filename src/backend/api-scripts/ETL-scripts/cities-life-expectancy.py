import json


def main():
    cities_json = []
    with open("../assets/cities.json", "r") as cities:
        cities_data = cities.read()

    with open("../assets/health.json", "r") as health:
        health_data = health.read()

    cities = json.loads(cities_data)
    health_cities = json.loads(health_data)

    for city in cities:
        city['life_expectancy'] = -999
        for health in health_cities:
            if health['city'] == city['city']:
                city['life_expectancy'] = health['life_expectancy']
        cities_json.append(city)

    with open("cities.json", "w") as cities_file:
        cities_file.write(json.dumps(cities_json))

    print(cities_json)


if __name__ == '__main__':
    main()
