import json


def main():
    with open('../assets/climate.json', 'r') as climate_file:
        climate_data = climate_file.read()

    with open('../assets/cities.json', 'r') as cities_file:
        cities_data = cities_file.read()

    # parse file
    cities = json.loads(cities_data)
    climates = json.loads(climate_data)

    for city in cities:
        city['average_ozone'] = -1
        for climate in climates:
            if climate['city'] == city['city']:
                city['average_ozone'] = climate['average_ozone']

    file = open("cities.json", "w")
    file.write(json.dumps(cities))
    print(json.dumps(cities))


if __name__ == '__main__':
    main()
