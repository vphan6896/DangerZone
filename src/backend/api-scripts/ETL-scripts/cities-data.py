import requests
import json


def main():
    # complete_url variable to store
    # complete url address
    complete_url = "https://gist.githubusercontent.com/Miserlou/c5cd8364bf9b2420bb29/raw/2bf258763cdddd704f8ffd3ea9a3e81d25e2c6f6/cities.json"

    # get method of requests module
    # return response object
    response = requests.get(complete_url)
    print(response)

    # json method of response object
    # convert json format data into
    # python format data
    json_obj = response.json()

    res = {}
    i = 0
    for dict in json_obj:
        json = {}
        json["city"] = dict['city']
        json["state"] = dict['state']
        json["population"] = dict['population']
        json["latitude"] = dict['latitude']
        json["longitude"] = dict['longitude']
        res[i] = json
        i += 1

    print(res)
    return res


if __name__ == '__main__':
    main()
