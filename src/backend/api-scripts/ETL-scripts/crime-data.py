import http
import json
from urllib.request import Request, urlopen
import os

debug = False

# documentation for the Crime-o-meter API can be found at:
# https://www.crimeometer.com/docs

CRIME_O_PUBLIC_KEY = os.getenv('CRIME_O_PUBLIC_KEY')
CRIME_O_SECRET_KEY = os.getenv('CRIME_O_SECRET_KEY')
BASE_URL = "https://api.crimeometer.com/v1/incidents"

headers = {
    'Content-Type': 'application/json',
    'x-api-key': CRIME_O_PUBLIC_KEY
}

# --- LOCATIONS --- #
"""
Required fields for getting crime stats for certain city:
    latitude
    longitude
    distance (radius from lat and long)
    datetime_ini
    datetime_end
    source
"""


def crime_stats(city: dict):
    query_request = BASE_URL + \
                    "/stats?lat=" + '{0:.4f}'.format(city['Latitude']) + \
                    "&lon=" + '{0:.4f}'.format(city['Longitude']) + \
                    "&distance=10mi&datetime_ini=2019-01-01T00:00:00.000Z&datetime_end=2020-01-02T00:00:00.000Z&source=1"

    request = Request(query_request, headers=headers)
    response_body = urlopen(request)
    json_obj = sanitize_response(response_body)
    res = {}
    res["city"] = city["city"]
    res["total_crime_incidents"] = json_obj["total_incidents"]
    res["murder"] = 0
    res["assault"] = 0
    res["drugs"] = 0
    res["burglary"] = 0
    res["vandalism"] = 0
    res["dui"] = 0
    res["sex_offenses"] = 0
    res["theft"] = 0
    res["animal_cruelty"] = 0
    res["other_offenses"] = 0

    city["total_crime_incidents"] = 0
    city["total_crime_incidents"] = json_obj["total_incidents"]

    report_types = json_obj["report_types"]
    for crime in report_types:
        if crime["type"] == "Murder & Non-negligent Manslaughter":
            res["murder"] = crime["count"]
        elif crime["type"] == "Aggravated Assault":
            res["assault"] = crime["count"]
        elif crime["type"] == "Drug/Narcotic Violations":
            res["drugs"] = crime["count"]
        elif crime["type"] == "Burglary/Breaking & Entering":
            res["burglary"] = crime["count"]
        elif crime["type"] == "Destruction/Damage/Vandalism of Property":
            res["vandalism"] = crime["count"]
        elif crime["type"] == "Driving Under the Influence":
            res["dui"] = crime["count"]
        elif crime["type"] == "Sex Offense/All Other":
            res["sex_offenses"] = crime["count"]
        elif crime["type"] == "Theft From Building":
            res["theft"] = crime["count"]
        elif crime["type"] == "Animal Cruelty":
            res["animal_cruelty"] = crime["count"]
        elif crime["type"] == "All Other Offenses":
            res["other_offenses"] = crime["count"]

    return res


def sanitize_response(response_body: http.client.HTTPResponse) -> dict:
    raw_data = response_body.read()
    encoding = response_body.info().get_content_charset('utf8')  # JSON default
    data: dict = json.loads(raw_data.decode(encoding))
    print(data) if debug else None
    return data


def get_crime_data():
    with open('../assets/cities.json', 'r') as cities:
        data = cities.read()

    # parse file
    crime_json = []
    cities = json.loads(data)

    i = 0
    for city in cities:
        if i == 100:
            break
        crime_json.append(crime_stats(city))
        i += 1

    # print(crime_json)
    file = open("../assets/cities.json", "w")
    file.write(json.dumps(cities))
    print(json.dumps(cities))


if __name__ == '__main__':
    get_crime_data()
