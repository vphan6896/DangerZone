import requests
import json
import os

# documentation for the Darksky API can be found at:
# https://darksky.net/dev/docs

DARKSKY_SECRET_KEY = os.getenv('DARKSKY_SECRET_KEY')
BASE_URL = "https://api.darksky.net/forecast/" + '7c18ad799feaf45567b20caba2129073'

# --- TIMES --- #
# 1/1/2019, 1/2/2019, 1/3/2019
timestamps = ["1546300800", "1546387200", "1546473600"]
timestamps_2019 = []

# --- LOCATIONS --- #
austin = {
    'lat': "30.264980",
    'long': "-97.746600"
}

houston = {
    'lat': "29.760427",
    'long': "-95.369804"
}

dallas = {
    'lat': "32.777980",
    'long': "-96.796210"
}

locations = [austin]

def get_weather_lat_lng_time(lat, long, time):
    response = requests.get(BASE_URL + "/" + lat + "," + long + "," + time)
    return response.json()

def output_weather(location, name):
    location_weather = {}

    print("Doing " + name)
    for time in timestamps_2019:
        location_weather[time] = get_weather_lat_lng_time( str(location['Latitude']), str(location['Longitude']), time)
        print(time + " date is added to " + name + " json.")
    print("Done with all days for " + name)

    with open(name + '_weatherFULL.json', 'w') as f:
        json.dump(location_weather, f, indent=4)
        print("Json dumped")

def main():
    seconds_in_day = 86400
    unix_2019_date = 1546300800
    days_in_year = 365

    for i in range(days_in_year):
        timestamps_2019.append( str(unix_2019_date + i * seconds_in_day) )

    print("Created timestamps")

    #load in all cities
    with open('../assets/cities.json') as f:
        cities = json.load(f)
        if cities:
            print("Loaded data correctly")

    for city in cities:
        output_weather( city, city.get("City") )
        print(city.get("City") + " has finished.")


    #LAST DID JOLIET

if __name__ == '__main__':
    main()