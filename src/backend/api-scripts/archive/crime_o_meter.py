import http
import json
from urllib.request import Request, urlopen
import os

debug = False

# documentation for the Crime-o-meter API can be found at:
# https://www.crimeometer.com/docs

CRIME_O_PUBLIC_KEY = os.getenv('CRIME_O_PUBLIC_KEY')
CRIME_O_SECRET_KEY = os.getenv('CRIME_O_SECRET_KEY')
BASE_URL = "https://api.crimeometer.com/v1/incidents"

# --- LOCATIONS --- #
"""
Required fields for getting crime stats for certain city:
    latitude
    longitude
    distance (radius from lat and long)
    datetime_ini
    datetime_end
    source
"""

dallas = {
    'lat': '32.7767',
    'long': '-96.7970',
    'distance': '10mi',
    'datetime_ini': '2019-01-01T00:00:00.000Z',
    'datetime_end': '2020-01-02T00:00:00.000Z',
    'source': '1',
}

houston = {
    'lat': '29.7604',
    'long': '-95.3698',
    'distance': '20mi',
    'datetime_ini': '2019-01-01T00:00:00.000Z',
    'datetime_end': '2020-01-02T00:00:00.000Z',
    'source': '1'
}

austin = {
    'lat': '30.2672',
    'long': '-97.7431',
    'distance': '10mi',
    'datetime_ini': '2019-01-01T00:00:00.000Z',
    'datetime_end': '2020-01-02T00:00:00.000Z',
    'source': '1'

}

headers = {
    'Content-Type': 'application/json',
    'x-api-key': CRIME_O_PUBLIC_KEY
}


def crime_stats(city: dict):
    query_request = BASE_URL + \
                    "/stats?lat=" + city['lat'] + \
                    "&lon=" + city['long'] + \
                    "&distance=" + city['distance'] + \
                    "&datetime_ini=" + city['datetime_ini'] + \
                    "&datetime_end=" + city['datetime_end'] + \
                    "&source=" + city['source']

    request = Request(query_request, headers=headers)
    response_body = urlopen(request)
    return response_body


def raw_data_coverage():
    query_request = BASE_URL + "/raw-data-coverage"
    request = Request(query_request, headers=headers)
    response_body = urlopen(request)
    return response_body


def raw_data_crowd_sourced(city: dict, page: str = '1'):
    query_request = BASE_URL + \
                    "/crowdsourced-raw-data?lat=" + city['lat'] + \
                    "&lon=" + city['long'] + \
                    "&distance=" + city['distance'] + \
                    "&datetime_ini=" + city['datetime_ini'] + \
                    "&datetime_end=" + city['datetime_end'] + \
                    "&page=" + page

    request = Request(query_request, headers=headers)
    response_body = urlopen(request)
    return response_body


def sanitize_response(response_body: http.client.HTTPResponse) -> dict:
    raw_data = response_body.read()
    encoding = response_body.info().get_content_charset('utf8')  # JSON default
    data: dict = json.loads(raw_data.decode(encoding))
    print(data) if debug else None
    return data


def write_to_sys(data, name):
    with open(name + '_crime.json', 'w') as f:
        json.dump(data, f)


def make_request():
    cities_response_body = raw_data_coverage()

    austin_response_body = crime_stats(austin)
    houston_response_body = crime_stats(houston)
    dallas_response_body = crime_stats(dallas)

    write_to_sys(sanitize_response(cities_response_body), "city")

    write_to_sys(sanitize_response(austin_response_body), "austin")
    write_to_sys(sanitize_response(houston_response_body), "houston")
    write_to_sys(sanitize_response(dallas_response_body), "dallas")

    austin_cs_response_body = raw_data_crowd_sourced(austin)
    houston_cs_response_body = raw_data_crowd_sourced(houston)
    dallas_cs_response_body = raw_data_crowd_sourced(dallas)

    # print(sanitize_response(austin_cs_response_body))
    # print(sanitize_response(houston_cs_response_body))
    # print(sanitize_response(dallas_cs_response_body))


if __name__ == '__main__':
    make_request()
