import json

states_to_timezones = {
    "alabama": "cst",
    "alaska": "akst",
    "arizona": "mst",
    "arkansas": "cst",
    "california": "pst",
    "colorado": "mst",
    "connecticut": "est",
    "delaware": "est",
    "district of columbia": "est",
    "florida": "est",
    "georgia": "est",
    "hawaii": "hst",
    "idaho": "mst",
    "illinois": "cst",
    "indiana": "est",
    "iowa": "cst",
    "kansas": "cst",
    "kentucky": "cst",
    "louisiana": "cst",
    "maine": "est",
    "maryland": "est",
    "massachusetts": "eset",
    "michigan": "est",
    "minnesota": "cst",
    "mississippi": "cst",
    "missouri": "cst",
    "montana": "mst",
    "nebraska": "cst",
    "nevada": "pst",
    "new hampshire": "est",
    "new jersey": "est",
    "new mexico": "mst",
    "new york": "est",
    "north carolina": "est",
    "north dakota": "cst",
    "ohio": "est",
    "oklahoma" : "cst",
    "oregon": "pst",
    "pennsylvania": "est",
    "rhode island": "est",
    "south carolina": "est",
    "south dakota": "cst",
    "tennessee": "cst",
    "texas": "cst",
    "utah": "mst",
    "vermont": "est",
    "virginia": "est",
    "washington": "pst",
    "west virginia": "est",
    "wisconsin": "cst",
    "wyoming": "mst",
}

f = open("cities.json","r+")
json_array = json.load(f)
for item in json_array:
    item["Time Zone"] = states_to_timezones[item["State"].lower()]
f.seek(0)
f.truncate()
f.write(json.dumps(json_array))
f.close()