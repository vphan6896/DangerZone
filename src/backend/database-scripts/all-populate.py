import json
import os
import ssl
import psycopg2


def main():
    cnx = ''
    try:
        connection = psycopg2.connect(user=os.environ['DB_USERNAME'],
                                      password=os.environ['DB_PASSWORD'],
                                      host=os.environ['DB_HOST'],
                                      port=os.environ['DB_PORT'],
                                      database=os.environ['DB_DATABASE'])

    except (Exception, psycopg2.Error) as error:
        print("ERROR: Could not connect to PostgreSQL DB")
        print("ERROR: " + str(error))
        raise error

    with open("../assets/cities.json", "r") as cities_json:
        cities_data = cities_json.read()

    with open("../assets/climate.json", "r") as climate_json:
        climate_data = climate_json.read()

    with open("../assets/crimes.json", "r") as crimes_json:
        crimes_data = crimes_json.read()

    with open("../assets/health.json", "r") as health_json:
        health_data = health_json.read()

    cities = json.loads(cities_data)
    climates = json.loads(climate_data)
    crimes = json.loads(crimes_data)
    health_city = json.loads(health_data)

    cur = connection.cursor()
    for city in cities:
        _city = city['city']
        state = city['state']
        population = city['population']
        latitude = city['latitude']
        longitude = city['longitude']
        time_zone = city['time_zone']
        total_crime_incidents = city['total_crime_incidents']
        average_high = city['average_high']
        average_low = city['average_low']
        life_expectancy = city['life_expectancy']
        average_ozone = city['average_ozone']

        try:

            # Prepared statement so no sql injections on incur
            insert_stmt = (
                "INSERT INTO cities (city, state, population, latitude, longitude, time_zone, total_crime_incidents, "
                "average_high, average_low, life_expectancy, average_ozone) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, "
                "%s, %s, %s)"
            )

            data = (
            _city, state, population, latitude, longitude, time_zone, total_crime_incidents, average_high, average_low,
            life_expectancy, average_ozone)

            cur.execute(insert_stmt, data)

        except (Exception, psycopg2.Error) as error:
            print("ERROR: Could not fulfill POST request")
            print("ERROR: " + str(error))
            connection.close()
            raise error

        connection.commit()
        print("POST request: Successfully executed command.")

    for climate in climates:
        climate['city'] = str(climate['city']).replace("'", "''")
        select_stmt = ("SELECT id FROM cities WHERE city='" + climate['city'] + "'")
        cur.execute(select_stmt)
        id_result = cur.fetchone()[0]

        city_id = id_result
        average_ozone = climate['average_ozone']

        spring_average_temp = climate['spring']['temperature']
        spring_wind_speed = climate['spring']['windSpeed']
        spring_humidity = climate['spring']['humidity']
        spring_uv = climate['spring']['uv']
        spring_ozone = climate['spring']['ozone']

        summer_average_temp = climate['summer']['temperature']
        summer_wind_speed = climate['summer']['windSpeed']
        summer_humidity = climate['summer']['humidity']
        summer_uv = climate['summer']['uv']
        summer_ozone = climate['summer']['ozone']

        fall_average_temp = climate['fall']['temperature']
        fall_wind_speed = climate['fall']['windSpeed']
        fall_humidity = climate['fall']['humidity']
        fall_uv = climate['fall']['uv']
        fall_ozone = climate['fall']['ozone']

        winter_average_temp = climate['winter']['temperature']
        winter_wind_speed = climate['winter']['windSpeed']
        winter_humidity = climate['winter']['humidity']
        winter_uv = climate['winter']['uv']
        winter_ozone = climate['winter']['ozone']

        try:

            # Prepared statement so no sql injections on incur
            insert_stmt = (
                "INSERT INTO climate (city_id, average_ozone, spring_average_temp, spring_wind_speed, "
                "spring_humidity, spring_uv, spring_ozone, summer_average_temp, summer_wind_speed, summer_humidity, "
                "summer_uv, summer_ozone, fall_average_temp, fall_wind_speed, fall_humidity, fall_uv, fall_ozone, "
                "winter_average_temp, winter_wind_speed, winter_humidity, winter_uv, winter_ozone) VALUES (%s, %s, "
                "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) "
            )

            data = (
                city_id, average_ozone, spring_average_temp, spring_wind_speed, spring_humidity, spring_uv, spring_ozone, summer_average_temp,
                summer_wind_speed, summer_humidity, summer_uv, summer_ozone, fall_average_temp, fall_wind_speed, fall_humidity, fall_uv, fall_ozone, winter_average_temp,
                winter_wind_speed, winter_humidity, winter_uv, winter_ozone
            )

            cur.execute(insert_stmt, data)

        except (Exception, psycopg2.Error) as error:
            print("ERROR: Could not fulfill POST request")
            print("ERROR: " + str(error))
            connection.close()
            raise error

        connection.commit()
        print("POST request: Successfully executed command.")

    for crime in crimes:
        crime['city'] = str(crime['city']).replace("'", "''")
        select_stmt = ("SELECT id FROM cities WHERE city='" + crime['city'] + "'")
        cur.execute(select_stmt)
        id_result = cur.fetchone()[0]

        city_id = id_result

        total_crime_incidents = crime['total_crime_incidents']
        murder = crime['murder']
        assault = crime['assault']
        drugs = crime['drugs']
        burglary = crime['burglary']
        vandalism = crime['vandalism']
        dui = crime['dui']
        sex_offenses = crime['sex_offenses']
        theft = crime['theft']
        animal_cruelty = crime['animal_cruelty']
        other_offenses = crime['other_offenses']

        try:

            # Prepared statement so no sql injections on incur
            insert_stmt = (
                "INSERT INTO crimes (city_id, total_crime_incidents, murder, assault, drugs, burglary, vandalism, dui, "
                "sex_offenses, theft, animal_cruelty, other_offenses) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
                "%s, %s) "
            )

            data = (
                city_id, total_crime_incidents, murder, assault, drugs, burglary, vandalism, dui,
                sex_offenses, theft, animal_cruelty, other_offenses
            )

            cur.execute(insert_stmt, data)

        except (Exception, psycopg2.Error) as error:
            print("ERROR: Could not fulfill POST request")
            print("ERROR: " + str(error))
            connection.close()
            raise error

        connection.commit()
        print("POST request: Successfully executed command.")

    for health in health_city:
        health['city'] = str(health['city']).replace("'", "''")
        select_stmt = ("SELECT id FROM cities WHERE city=\'" + health['city'] + "\'")

        cur.execute(select_stmt)
        id_result = cur.fetchone()[0]

        city_id = id_result

        air_pollution_particle = health['air_pollution_particle']
        obesity = health['obesity']
        frequent_mental_distress = health['frequent_mental_distress']
        smoking = health['smoking']
        diabetes = health['diabetes']
        walkability = health['walkability']
        opioid_overdose_deaths = health['opioid_overdose_deaths']
        life_expectancy = health['life_expectancy']
        limited_access_to_healthy_foods = health['limited_access_to_healthy_foods']
        cardiovascular_disease_deaths = health['cardiovascular_disease_deaths']
        lead_exposure_risk_index = health['lead_exposure_risk_index']

        try:

            # Prepared statement so no sql injections on incur
            insert_stmt = (
                "INSERT INTO health (city_id, air_pollution_particle, obesity, frequent_mental_distress, smoking, "
                "diabetes, walkability, opioid_overdose_deaths, life_expectancy, limited_access_to_healthy_foods, "
                "cardiovascular_disease_deaths, lead_exposure_risk_index) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, "
                "%s, %s, %s) "
            )

            data = (
                city_id, air_pollution_particle, obesity, frequent_mental_distress, smoking, diabetes, walkability,
                opioid_overdose_deaths, life_expectancy, limited_access_to_healthy_foods,
                cardiovascular_disease_deaths, lead_exposure_risk_index
            )

            cur.execute(insert_stmt, data)

        except (Exception, psycopg2.Error) as error:
            print("ERROR: Could not fulfill POST request")
            print("ERROR: " + str(error))
            connection.close()
            raise error

        connection.commit()
        print("POST request: Successfully executed command.")

    connection.close()


if __name__ == '__main__':
    main()
