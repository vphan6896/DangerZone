import json
import os
import psycopg2


def main():
    cnx = ''
    try:
        connection = psycopg2.connect(user=os.environ['DB_USERNAME'],
                                      password=os.environ['DB_PASSWORD'],
                                      host=os.environ['DB_HOST'],
                                      port=os.environ['DB_PORT'],
                                      database=os.environ['DB_DATABASE'])

    except (Exception, psycopg2.Error) as error:
        print("ERROR: Could not connect to PostgreSQL DB")
        print("ERROR: " + str(error))
        raise error

    with open("../assets/crimes.json", "r") as crimes_json:
        data = crimes_json.read()

    crimes = json.loads(data)

    cur = connection.cursor()
    for crime in crimes:
        crime['city'] = str(crime['city']).replace("'", "''")
        select_stmt = ("SELECT id FROM cities WHERE city='" + crime['city'] + "'")
        cur.execute(select_stmt)
        id_result = cur.fetchone()[0]

        city_id = id_result

        total_crime_incidents = crime['total_crime_incidents']
        murder = crime['murder']
        assault = crime['assault']
        drugs = crime['drugs']
        burglary = crime['burglary']
        vandalism = crime['vandalism']
        dui = crime['dui']
        sex_offenses = crime['sex_offenses']
        theft = crime['theft']
        animal_cruelty = crime['animal_cruelty']
        other_offenses = crime['other_offenses']

        try:

            # Prepared statement so no sql injections on incur
            insert_stmt = (
                "INSERT INTO crimes (city_id, total_crime_incidents, murder, assault, drugs, burglary, vandalism, dui, "
                "sex_offenses, theft, animal_cruelty, other_offenses) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
                "%s, %s) "
            )

            data = (
                city_id, total_crime_incidents, murder, assault, drugs, burglary, vandalism, dui,
                sex_offenses, theft, animal_cruelty, other_offenses
            )

            cur.execute(insert_stmt, data)

        except (Exception, psycopg2.Error) as error:
            print("ERROR: Could not fulfill POST request")
            print("ERROR: " + str(error))
            connection.close()
            raise error

        connection.commit()
        print("POST request: Successfully executed command.")

    connection.close()


if __name__ == '__main__':
    main()
