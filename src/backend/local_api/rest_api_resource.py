# Resources

from flask import Flask, jsonify, abort, request, render_template
from .cities_service import CitiesService
from .health_service import HealthService
from .climate_service import ClimateService
from .crime_service import CrimeService
from .database_domain import test_DB_connection, get_postgresql_connection
from .schemas import Session
from flask_cors import CORS, cross_origin

app = Flask(__name__)
app.config['CORS_HEADERS'] = 'Content-Type'
CORS(app)
engine = get_postgresql_connection()
Session.configure(bind=engine)

# Presentation - Defining entry Point
@app.route('/crime', methods=['GET'])
@cross_origin(origin='*', headers=['Content-Type', 'Authorization'])
def get_cities_or_city_crime():
    service = CrimeService(engine)
    query_string = request.query_string.decode("utf-8")
    print(query_string)
    print(request.args)

    if request.method == 'GET':
        if query_string == "":
            climate = service.get_all_cities_crimes()
            return jsonify(climate)

        if len(request.args) == 1:
            try:
                attribute = next(iter(request.args)).lower()
                param_value = request.args[attribute].lower()
                if attribute == 'city' and param_value != '':
                    result = service.get_city_crime(param_value)
                    return jsonify(result)
                else:
                    abort(400)
            except Exception as err:
                print(err)
                abort(400)
        elif len(request.args) == 2:
            try:
                iterator = iter(request.args)
                attribute1 = next(iterator).lower()
                param_value1 = request.args[attribute1].lower()
                attribute2 = next(iterator).lower()
                param_value2 = request.args[attribute2].lower()
                if attribute1 == 'offset' and param_value1 != '' and attribute2 == 'limit' and param_value2 != '':
                    result = service.get_all_cities_crime_paginate(param_value1,param_value2)
                    return jsonify(result)
                elif attribute2 == 'offset' and param_value2 != '' and attribute1 == 'limit' and param_value1 != '':
                    result = service.get_all_cities_crime_paginate(param_value2,param_value1)
                    return jsonify(result)
                else:
                    abort(400)
            except Exception as err:
                print(err)
                abort(400)
        elif len(request.args) == 4:
            try:
                args = request.args
                return jsonify(service.get_all_cities_crime_sort_paginate(args["column"],
                                                                    args["asc"],
                                                                    args["offset"],
                                                                    args["limit"]))
            except Exception as err:
                print(err)
                abort(400)

        else:
            abort(400)
    else:
        abort(400)

@app.route('/crime', methods=['POST'])
@cross_origin(origin='*', headers=['Content-Type', 'Authorization'])
def get_cities_crime_ids():
    service = CrimeService(engine)
    json = request.json
    crime_ids = json["crime_ids"]
    return jsonify(service.get_city_crime_ids(crime_ids))

@app.route('/crime/worst', methods=['GET'])
@cross_origin(origin='*', headers=['Content-Type', 'Authorization'])
def get_worst_three_city_crimes():
    service = CrimeService(engine)
    query_string = request.query_string.decode("utf-8")
    print(query_string)
    print(request.args)

    if request.method == 'GET':
        if query_string == "":
            abort(400)

        if len(request.args) == 1:
            try:
                attribute = next(iter(request.args)).lower()
                param_value = request.args[attribute].lower()
                if attribute == 'city' and param_value != '':
                    result = service.get_worst_three_city_crimes(param_value)
                    return jsonify(result)
                else:
                    abort(400)
            except Exception as err:
                print(err)
                abort(400)
        else:
            abort(400)
    else:
        abort(400)


@app.route('/crime/filter', methods=['GET'])
@cross_origin(origin='*', headers=['Content-Type', 'Authorization'])
def get_city_crime_by_filter():
    service = CrimeService(engine)
    query_string = request.query_string.decode("utf-8")
    print(query_string)
    print(request.args)

    if request.method == 'GET':
        if query_string == "":
            abort(400, "error in request")

        if len(request.args) == 1:
            try:
                attribute = next(iter(request.args)).lower()
                param_value = request.args[attribute].lower()
                if attribute != '' and param_value != '':
                    result = service.get_city_crime_by_filter(attribute, param_value)
                    return jsonify(result)
                else:
                    abort(400)
            except Exception as err:
                print(err)
                abort(400)
        else:
            abort(400)
    else:
        abort(400)


# Presentation - Defining entry Point
@app.route('/climate', methods=['GET'])
@cross_origin(origin='*', headers=['Content-Type', 'Authorization'])
def get_cities_or_city_climate():
    service = ClimateService(engine)
    query_string = request.query_string.decode("utf-8")
    print(query_string)
    print(request.args)

    if request.method == 'GET':
        if query_string == "":
            climate = service.get_all_cities_climate()
            return jsonify(climate)

        if len(request.args) == 1:
            try:
                attribute = next(iter(request.args)).lower()
                param_value = request.args[attribute].lower()
                if attribute == 'season' and param_value != '':
                    result = service.get_climate_by_season(param_value)
                    return jsonify(result)
                if attribute == 'city' and param_value != '':
                    result = service.get_climate_by_city(param_value)
                    return jsonify(result)
                else:
                    abort(400)
            except Exception as err:
                print(err)
                abort(400)

        if len(request.args) == 2:
            try:
                req_args = iter(request.args)
                attribute1 = next(req_args).lower()
                param_value1 = request.args[attribute1].lower()


                attribute2 = next(req_args).lower()
                param_value2 = request.args[attribute2].lower()

                if (attribute1 == 'city' and attribute2 == 'season'  and param_value1 != '' and param_value2 != '') \
                        or (attribute2 == 'city' and attribute1 == 'season'  and param_value1 != '' and param_value2 != ''):
                    result = service.get_climate_by_city_and_season(attribute1, attribute2, param_value1, param_value2)
                    return jsonify(result)
                elif attribute1 == 'offset' and param_value1 != '' and attribute2 == 'limit' and param_value2 != '':
                    result = service.get_all_cities_climate_paginate(param_value1, param_value2)
                    return jsonify(result)
                elif attribute2 == 'offset' and param_value2 != '' and attribute1 == 'limit' and param_value1 != '':
                    result = service.get_all_cities_climate_paginate(param_value2, param_value1)
                    return jsonify(result)
                else:
                    abort(400)
            except Exception as err:
                print(err)
                abort(400)
        if len(request.args) == 4:
            try:
                args = request.args
                return jsonify(service.get_all_cities_climate_sort_paginate(args["column"],
                                                                    args["asc"],
                                                                    args["offset"],
                                                                    args["limit"]))
            except Exception as err:
                print(err)
                abort(400)
        else:
            abort(400)
    else:
        abort(400)


@app.route('/climate', methods=['POST'])
@cross_origin(origin='*', headers=['Content-Type', 'Authorization'])
def get_cities_climate_ids():
    service = ClimateService(engine)
    json = request.json
    crime_ids = json["climate_ids"]
    return jsonify(service.get_city_climate_ids(crime_ids))

# Presentation - Defining entry Point
@app.route('/health', methods=['GET'])
@cross_origin(origin='*', headers=['Content-Type', 'Authorization'])
def get_cities_or_city_health():
    service = HealthService(engine)
    query_string = request.query_string.decode("utf-8")
    print(query_string)
    print(request.args)

    if request.method == 'GET':
        if query_string == "":
            health_cities = service.get_all_cities_health()
            return jsonify(health_cities)

        if len(request.args) == 1:
            try:
                attribute = next(iter(request.args)).lower()
                param_value = request.args[attribute].lower()
                if attribute == 'city' and param_value != '':
                    result = service.get_health_by_city(param_value)
                    return jsonify(result)
                else:
                    abort(400)
            except Exception as err:
                print(err)
                abort(400)
        elif len(request.args) == 2:
            try:
                iterator = iter(request.args)
                attribute1 = next(iterator).lower()
                param_value1 = request.args[attribute1].lower()
                attribute2 = next(iterator).lower()
                param_value2 = request.args[attribute2].lower()
                if attribute1 == 'offset' and param_value1 != '' and attribute2 == 'limit' and param_value2 != '':
                    result = service.get_all_cities_health_paginate(param_value1,param_value2)
                    return jsonify(result)
                elif attribute2 == 'offset' and param_value2 != '' and attribute1 == 'limit' and param_value1 != '':
                    result = service.get_all_cities_health_paginate(param_value2,param_value1)
                    return jsonify(result)
                else:
                    abort(400)
            except Exception as err:
                print(err)
                abort(400)
        elif len(request.args) == 4:
            try:
                args = request.args
                return jsonify(service.get_all_cities_health_sort_paginate(args["column"],
                                                                    args["asc"],
                                                                    args["offset"],
                                                                    args["limit"]))
            except Exception as err:
                print(err)
                abort(400)
        else:
            abort(400)
    else:
        abort(400)


@app.route('/health', methods=['POST'])
@cross_origin(origin='*', headers=['Content-Type', 'Authorization'])
def get_cities_health_ids():
    service = HealthService(engine)
    json = request.json
    crime_ids = json["health_ids"]
    return jsonify(service.get_city_health_ids(crime_ids))

@app.route('/health/filter', methods=['GET'])
@cross_origin(origin='*', headers=['Content-Type', 'Authorization'])
def get_city_health_by_filter():
    service = HealthService(engine)
    query_string = request.query_string.decode("utf-8")
    print(query_string)
    print(request.args)

    if request.method == 'GET':
        if query_string == "":
            abort(400)

        if len(request.args) == 1:
            try:
                attribute = next(iter(request.args)).lower()
                param_value = request.args[attribute].lower()
                if attribute != '' and param_value != '':
                    result = service.get_city_health_by_filter(attribute, param_value)
                    return jsonify(result)
                else:
                    abort(400)
            except Exception as err:
                print(err)
                abort(400)
        else:
            abort(400)
    else:
        abort(400)


# Presentation - Defining entry Point
@app.route('/cities', methods=['GET'])
@cross_origin(origin='*', headers=['Content-Type', 'Authorization'])
def get_cities_or_city():
    service = CitiesService(engine)
    query_string = request.query_string.decode("utf-8")
    print(query_string)
    print(request.args)

    if request.method == 'GET':
        if query_string == "":
            cities = service.get_all_cities()
            return jsonify(cities)

        if len(request.args) == 1:
            try:
                attribute = next(iter(request.args)).lower()
                param_value = request.args[attribute].lower()
                if attribute == 'city' and param_value != '':
                    result = service.get_city(param_value)
                    return jsonify(result)
                else:
                    abort(400)
            except Exception as err:
                print(err)
                abort(400)
        elif len(request.args) == 2:
            try:
                iterator = iter(request.args)
                attribute1 = next(iterator).lower()
                param_value1 = request.args[attribute1].lower()
                attribute2 = next(iterator).lower()
                param_value2 = request.args[attribute2].lower()
                if attribute1 == 'offset' and param_value1 != '' and attribute2 == 'limit' and param_value2 != '':
                    result = service.get_all_cities_paginate(param_value1,param_value2)
                    return jsonify(result)
                elif attribute2 == 'offset' and param_value2 != '' and attribute1 == 'limit' and param_value1 != '':
                    result = service.get_all_cities_paginate(param_value2,param_value1)
                    return jsonify(result)
                else:
                    abort(400)
            except Exception as err:
                print(err)
                abort(400)
        elif len(request.args) == 4:
            try:
                args = request.args
                return jsonify(service.get_all_cities_sort_paginate(args["column"],
                                                                    args["asc"],
                                                                    args["offset"],
                                                                    args["limit"]))
            except Exception as err:
                print(err)
                abort(400)
        else:
            abort(400)
    else:
        abort(400)

@app.route('/city', methods=['POST'])
@cross_origin(origin='*', headers=['Content-Type', 'Authorization'])
def get_cities_ids():
    service = CitiesService(engine)
    json = request.json
    crime_ids = json["city_ids"]
    return jsonify(service.get_city_ids(crime_ids))


@app.route('/cities/filter', methods=['GET'])
@cross_origin(origin='*', headers=['Content-Type', 'Authorization'])
def get_cities_by_filter():
    service = CitiesService(engine)
    query_string = request.query_string.decode("utf-8")
    print(query_string)
    print(request.args)

    if request.method == 'GET':
        if len(request.args) == 1:
            try:
                attribute = next(iter(request.args)).lower()
                param_value = request.args[attribute].lower()
                attribute = attribute.lower()
                if attribute != '' and param_value != '':
                    result = service.get_cities_by_attribute(attribute, param_value)
                    return jsonify(result)
                else:
                    abort(400)
            except Exception as err:
                print(err)
                abort(400)
        else:
            abort(400)
    else:
        abort(400)


@app.route('/', methods=['GET'])
@cross_origin(origin='*', headers=['Content-Type', 'Authorization'])
def landing():
    #print(test_DB_connection())
    message = "Welcome to the Dangerzone API!<br/><br/>" \
              "To get started, please go to this link for more information on how to make API requests:<br/>" \
              "<a href=\"https://documenter.getpostman.com/view/10517580/SzKYPGyC?version=latest\">" \
              "https://documenter.getpostman.com/view/10517580/SzKYPGyC?version=latest</a>"

    return message

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)