from sqlalchemy import Column, Integer, String, Float, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

Base = declarative_base()
Session = sessionmaker(autoflush=False)

class City(Base):
    __tablename__ = 'cities'

    id = Column(Integer, primary_key=True)
    city = Column(String)
    state = Column(String)
    population = Column(Integer)
    time_zone = Column(String)
    latitude = Column(Float)
    longitude = Column(Float)
    average_high = Column(Integer)
    average_low = Column(Integer)
    average_ozone = Column(Integer)
    life_expectancy = Column(Float)
    total_crime_incidents = Column(Integer)

class Crime(Base):
    __tablename__ = 'crimes'

    id = Column(Integer, primary_key=True)
    city_id = Column(Integer, ForeignKey('cities.id'))
    total_crime_incidents = Column(Integer)
    murder = Column(Integer)
    assault = Column(Integer)
    drugs = Column(Integer)
    burglary = Column(Integer)
    vandalism = Column(Integer)
    dui = Column(Integer)
    sex_offenses = Column(Integer)
    theft = Column(Integer)
    animal_cruelty = Column(Integer)
    other_offenses = Column(Integer)

class Health(Base):
    __tablename__ = 'health'

    id = Column(Integer, primary_key=True)
    city_id = Column(Integer, ForeignKey('cities.id'))
    air_pollution_particle = Column(Float)
    cardiovascular_disease_deaths = Column(Float)
    frequent_mental_distress = Column(Float)
    lead_exposure_risk_index = Column(Float)
    opioid_overdose_deaths = Column(Float)
    smoking = Column(Float)
    diabetes = Column(Float)
    obesity = Column(Float)
    life_expectancy = Column(Float)
    limited_access_to_healthy_foods = Column(Float)
    walkability = Column(Float)

class Climate(Base):
    __tablename__ = 'climate'

    id = Column(Integer, primary_key=True)
    city_id = Column(Integer, ForeignKey('cities.id'))
    average_ozone = Column(Float)
    spring_average_temp = Column(Float)
    spring_wind_speed = Column(Float)
    spring_humidity = Column(Float)
    spring_uv = Column(Float)
    spring_ozone = Column(Float)
    summer_average_temp = Column(Float)
    summer_wind_speed = Column(Float)
    summer_humidity = Column(Float)
    summer_uv = Column(Float)
    summer_ozone = Column(Float)
    fall_average_temp = Column(Float)
    fall_wind_speed = Column(Float)
    fall_humidity = Column(Float)
    fall_uv = Column(Float)
    fall_ozone = Column(Float)
    winter_average_temp = Column(Float)
    winter_wind_speed = Column(Float)
    winter_humidity = Column(Float)
    winter_uv = Column(Float)
    winter_ozone = Column(Float)
