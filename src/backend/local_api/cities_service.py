# Services - Business Logic
# Data Access Object - Database Query Logic
# Tightly coupled in this case
from flask import abort
from sqlalchemy import Table, select, column, desc, asc
import sqlalchemy
from .schemas import Base, Session, City


class CitiesService:
    def __init__(self, engine):
        self.engine = engine

    def get_all_cities(self):
        connection = self.engine.connect()

        table = Table('cities', Base.metadata, autoload=True)
        stmt = select([table])
        query_result = connection.execute(stmt)

        cities = []
        for row in query_result:
            city = {
                'city': row[1],
                'state': row[2],
                'population': row[3],
                'time_zone': row[4],
                'latitude': row[5],
                'longitude': row[6],
                'average_high': row[7],
                'average_low': row[8],
                'average_ozone': row[9],
                'life_expectancy': row[10]
            }
            cities.append(city)

        connection.close()

        return cities

    def get_all_cities_paginate(self, offset, limit):
        session = Session()
        query_result = session.query(City).limit(limit).offset(offset).all()
        cities = []
        for row in query_result:
            city = {
                'city': row.city,
                'state': row.state,
                'population': row.population,
                'time_zone': row.time_zone,
                'latitude': row.latitude,
                'longitude': row.longitude,
                'average_high': row.average_high,
                'average_low': row.average_low,
                'average_ozone': row.average_ozone,
                'life_expectancy': row.life_expectancy
            }
            cities.append(city)
        session.close()

        return cities

    def get_all_cities_sort_paginate(self, column, asc, offset, limit):
        session = Session()

        order = None
        if(asc.lower() == 'true'):
            order = sqlalchemy.asc
        else:
            order = sqlalchemy.desc

        query_result = session.query(City).order_by(order(City.__dict__[column])).limit(limit).offset(offset).all()
        cities = []
        for row in query_result:
            city = {
                'city': row.city,
                'state': row.state,
                'population': row.population,
                'time_zone': row.time_zone,
                'latitude': row.latitude,
                'longitude': row.longitude,
                'average_high': row.average_high,
                'average_low': row.average_low,
                'average_ozone': row.average_ozone,
                'life_expectancy': row.life_expectancy
            }
            cities.append(city)
        session.close()

        return cities

    def get_city(self, param_value):
        connection = self.engine.connect()

        table = Table('cities', Base.metadata, autoload=True)
        stmt = select([table]).where(column('city') == param_value)
        try:
            query_result = connection.execute(stmt)
            city = []
            for row in query_result:
                sanitize_data = {
                    'city': row[1],
                    'state': row[2],
                    'population': row[3],
                    'time_zone': row[4],
                    'latitude': row[5],
                    'longitude': row[6],
                    'average_high': row[7],
                    'average_low': row[8],
                    'average_ozone': row[9],
                    'life_expectancy': row[10]
                }
            city.append(sanitize_data)
            connection.close()
        except Exception as error:
            print(error)
            connection.close()
            abort(404)

        if city is None:
            connection.close()
            abort(404)

        connection.close()
        return city


    def get_city_ids(self, city_ids):
        session = Session()

        try:
            query_result = session.query(City).filter(City.id.in_(city_ids)).all()
            if query_result is None:
                session.close()
                abort(404)

            cities = []
            for row in query_result:
                city = {
                    'city': row.city,
                    'state': row.state,
                    'population': row.population,
                    'time_zone': row.time_zone,
                    'latitude': row.latitude,
                    'longitude': row.longitude,
                    'average_high': row.average_high,
                    'average_low': row.average_low,
                    'average_ozone': row.average_ozone,
                    'life_expectancy': row.life_expectancy
                }
                cities.append(city)
            session.close()

            return cities
        except Exception as error:
            print(error)
            session.close()
            abort(400)

    def get_cities_by_attribute(self, attribute, param_value):
        connection = self.engine.connect()

        table = Table('cities', Base.metadata, autoload=True)
        stmt = select([table]).where(column(attribute) == param_value)

        try:
            query_result = connection.execute(stmt)

            cities = []
            for row in query_result:
                city = {
                    'city': row[1],
                    'state': row[2],
                    'population': row[3],
                    'time_zone': row[4],
                    'latitude': row[5],
                    'longitude': row[6],
                    'average_high': row[7],
                    'average_low': row[8],
                    'average_ozone': row[9],
                    'life_expectancy': row[10]
                }
                cities.append(city)
        except Exception as error:
            print(error)
            connection.close()
            abort(400)

        connection.close()

        return cities

