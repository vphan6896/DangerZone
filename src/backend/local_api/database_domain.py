# Domain

import os
from sqlalchemy import create_engine

engine = None

def test_DB_connection():
    connection = None
    try:
        connection = engine.connect()
        print("You are connected to Postgres")

    except (Exception) as error:
        print("Error: Could not connect to PostgreSQL")
        print(error)
        raise error
    finally:
        # closing database connection.
        if connection:
            connection.close()
            print("PostgreSQL connection is closed")

    return "Successful connection"


def get_postgresql_connection():
    try:
        engine = create_engine('postgresql://{user}:{password}@{host}:{port}/{database}'.format(
            user=os.environ['DB_USERNAME'],
            password=os.environ['DB_PASSWORD'],
            host=os.environ['DB_HOST'],
            port=os.environ['DB_PORT'],
            database=os.environ['DB_DATABASE']))

    except Exception as error:
        print("ERROR: Could not connect to PostgreSQL DB")
        print("ERROR: " + str(error))
        raise error

    return engine
