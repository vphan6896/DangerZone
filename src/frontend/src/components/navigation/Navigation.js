import React, {Component} from 'react'
import {Button, Form, FormControl, Navbar} from "react-bootstrap";
import {Nav} from "react-bootstrap";

class Navigation extends Component {
    render() {
        return (
            <div>
                <Navbar className="color-nav" variant="dark">
                    <Navbar.Brand href="/">
                        <img
                            alt="Danger Zone Icon"
                            src="https://icons.iconarchive.com/icons/ampeross/qetto-2/32/danger-icon.png"
                            width="28"
                            height="28"
                            className="d-inline-block align-top"
                        />{' '}
                        DangerZone</Navbar.Brand>
                    <Nav className="mr-auto">
                        <Nav.Link href="/home">Home</Nav.Link>
                        <Nav.Link href="/cities">Cities</Nav.Link>
                        <Nav.Link href="/climate">Climate</Nav.Link>
                        <Nav.Link href="/health">Health</Nav.Link>
                        <Nav.Link href="/crime">Crime</Nav.Link>
                        <Nav.Link
                            href="https://documenter.getpostman.com/view/10517580/SzKYPGyC?version=latest">API</Nav.Link>
                        <Nav.Link href="/visualizations">Visualizations</Nav.Link>
                        <Nav.Link href="/about">About</Nav.Link>
                    </Nav>
                    <Form inline>
                        <FormControl type="text" placeholder="Search" className="mr-sm-2"/>
                        <Button variant="outline-info">Search</Button>
                    </Form>
                </Navbar>
            </div>
        );
    }
}

export default Navigation