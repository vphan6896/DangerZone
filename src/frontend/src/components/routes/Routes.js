import React, {Component} from "react";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Cities from "../cities/Cities";
import Climate from "../climate/Climate";
import Crime from "../crime/Crime";
import Health from "../health/Health";
import Visualizations from "../visualizations/Visualizations";
import About from "../about/About";
import Home from "../home/Home";

class Routes extends Component {
    render() {
        return (
            <Router>
                <Switch>

                    <Route path="/cities/">
                        <Cities/>
                    </Route>

                    <Route path="/climate/">
                        <Climate/>
                    </Route>

                    <Route path="/crime/">
                        <Crime/>
                    </Route>

                    <Route path="/health/">
                        <Health/>
                    </Route>

                    <Route path="/visualizations/">
                        <Visualizations/>
                    </Route>

                    <Route path="/about/">
                        <About/>
                    </Route>

                    <Route path="/">
                        <Home/>
                    </Route>

                </Switch>
            </Router>
        );
    }
}

export default Routes;