import { Gitlab } from 'gitlab';

const debug = true;

if(!goodEnv()) {
    console.error("Environmental variables are not set correctly");
    throw "Please fix environmental variables before proceeding";
}

const api: Gitlab = new Gitlab({
    token: process.env.GITLAB_TOKEN,
    host: process.env.GITLAB_HOST
});

const projectId: number = parseInt(process.env.GITLAB_PROJECT_ID!);
const memberIds: Array<number> = [
    parseInt(process.env.GITLAB_ID_0!),
    parseInt(process.env.GITLAB_ID_1!),
    parseInt(process.env.GITLAB_ID_2!),
    parseInt(process.env.GITLAB_ID_3!),
    parseInt(process.env.GITLAB_ID_4!)
];

// Hard-coded numbers of unit tests members have made
const unitTests: Array<number> = [
    0,  // Kevin
    0,  // Vincent
    0,  // Andy
    0,  // Vy
    0   // Preston
];

export interface MemberStats {
    id: number;
    name: string;
    username: string;
    commitsMade: number;
    issuesClosed: number;
    mergeRequestsMade: number;
    unitTests: number;
}

// TODO: Get Project member's: - DONE
//  names
//  username
//  commits
//  issues closed
//  merge requests
export async function about(): Promise<Array<MemberStats>> {
    const groupMembers: Array<MemberStats> = populateGroupMembers(memberIds);

    // parse name and gitlab username of team members
    await Promise.all(memberIds.map(async id => {
        await api.ProjectMembers.show(projectId, id)
            .then(res => {
                const member = JSON.parse(JSON.stringify(res));
                groupMembers.find(mem => {
                    if(mem.id === member.id) {
                        mem.name = member.name;
                        mem.username = member.username;
                        return true;
                    }
                    return false;
                });
            })
            .catch(err => console.error(err));
        }),
    );

    // parse all commits made by group members
    await api.Commits.all(projectId)
        .then(res => {
            const commits = JSON.parse(JSON.stringify(res));
            for(let i = 0; i < commits.length; i++) {
                groupMembers.find(member => {
                    if(member.name === commits[i].author_name) {
                        member.commitsMade++;
                        return true;
                    }
                    return false;
                });
            }
        })
        .catch(err => console.error(err));

    // parse all issues closed by group members
    await api.Issues.all({projectId})
        .then(res => {
            const issues = JSON.parse(JSON.stringify(res));
            for(let i = 0; i < issues.length; i++) {
                groupMembers.find(member => {
                    if(issues[i].state === 'closed') {
                        if(member.name === issues[i].closed_by.name) {
                            member.issuesClosed++;
                            return true;
                        }
                    }
                    return false;
                });
            }
        })
        .catch(err => console.error(err));;

    // parse all merge requests made by group members
    await api.MergeRequests.all({projectId})
        .then(res => {
            const mergeRequests = JSON.parse(JSON.stringify(res));
            for(let i = 0; i < mergeRequests.length; i++) {
                groupMembers.find(member => {
                    if(member.name === mergeRequests[i].author.name) {
                        member.mergeRequestsMade++;
                        return true;
                    }
                    return false;
                });
            }
        })
        .catch(err => console.error(err));;

    debug ? console.log("Group Member data:\n", groupMembers) : null;
    return groupMembers;
}

function populateGroupMembers(memberIds: Array<number>): Array<MemberStats> {
    const groupMembers: Array<MemberStats> = new Array<MemberStats>();
    for(let i = 0; i < memberIds.length; i++) {
        const member: MemberStats = {
            id: memberIds[i],
            name: '',
            username: '',
            commitsMade: 0,
            issuesClosed: 0,
            mergeRequestsMade: 0,
            unitTests: unitTests[i]
        };
        groupMembers.push(member);
    }

    return groupMembers;
}

function goodEnv() {
    if(typeof process.env.GITLAB_TOKEN === "undefined" ||
        typeof process.env.GITLAB_HOST === "undefined" ||
        typeof process.env.GITLAB_PROJECT_ID === "undefined" ||
        typeof process.env.GITLAB_ID_0 === "undefined" ||
        typeof process.env.GITLAB_ID_1 === "undefined" ||
        typeof process.env.GITLAB_ID_2 === "undefined" ||
        typeof process.env.GITLAB_ID_3 === "undefined" ||
        typeof process.env.GITLAB_ID_4 === "undefined" ||
        isNaN(parseInt(process.env.GITLAB_PROJECT_ID)) ||
        isNaN(parseInt(process.env.GITLAB_ID_0)) ||
        isNaN(parseInt(process.env.GITLAB_ID_1)) ||
        isNaN(parseInt(process.env.GITLAB_ID_2)) ||
        isNaN(parseInt(process.env.GITLAB_ID_3)) ||
        isNaN(parseInt(process.env.GITLAB_ID_4))
    ) {
        return false;
    }
    return true;
}

// returns issues stats object with attributes with all, closed, opened issues
async function getIssues(): Promise<{all: number, closed: number, opened: number}> {
    return await api.IssuesStatistics.all({projectId})
        .then(res => {
            return JSON.parse(JSON.stringify(res))['statistics']['counts'];
        });
}

async function getTotalIssues() {
    const issuesStats = await getIssues();
    // debug ? console.log("Total issues is:", issuesStats.all) : null;
    return issuesStats.all;
}

async function getIssuesClosed() {
    const issuesStats = await getIssues();
    // debug ? console.log("Total issues closed is:", issuesStats.closed) : null;
    return issuesStats.closed;
}

async function getIssuesOpened() {
    const issuesStats = await getIssues();
    // debug ? console.log("Total issues opened is:", issuesStats.opened) : null;
    return issuesStats.opened;
}

async function getTotalCommits(): Promise<number> {
    const commits = await api.Commits.all(projectId)
        .then(res => {
            return JSON.parse(JSON.stringify(res));
        });

    // debug ? console.log("Total commits is:", commits) : null;

    return Object.keys(commits).length;
}

async function testGitLabAPI(): Promise<void> {
    // Project's overall metadata
    console.log(await api.Projects.show(projectId));

    // MergeRequests - shows all PR's including if it has been closed and its author that made it
    console.log(await api.MergeRequests.all({projectId}));

    // Commits/Contributors in a brief fashion
    console.log(await api.Repositories.contributors(projectId));

    // All project members including other roles besides maintainers (actual developers)
    console.log(await api.ProjectMembers.all(projectId));

    // Overall issues statistics
    // i.e. { statistics: { counts: { all: number, closed: number, opened: number } } }
    console.log(await api.IssuesStatistics.all({projectId}));

    // Show's all isses including closed ones to quantify who closed what
    console.log(await api.Issues.all({projectId}));

    // Shows all commits and more detail of each one including the author_name
    console.log(await api.Commits.all(projectId));

    // shows a particular user's information based on their gitlab ID
    console.log(await api.Users.show(5109816));
}

about().then(r => "");