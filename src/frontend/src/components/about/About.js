import React, {Component} from 'react'
import 'bootstrap/dist/css/bootstrap-grid.min.css';
import {getGitlabStats} from "./fetch_v2";
import "./About.css";

class About extends Component {
    constructor() {
        super();
        this.state = {
            result: null
        };
    }

    componentDidMount() {
        getGitlabStats().then((res) => {
            this.setState({
                result: res
            })
        });
    }

    render() {
        return (
            <div className="container text-center">
                <h1>About</h1>

                <h3>Mission Statement</h3>
                <p>Want to find the best city? Curious how a city stands compared to others? Danger Zone displays useful
                    information to answer these questions, as well as give insights what specifically could be wrong
                    with a city. Many aspects of ongoings in cities are provided: weather information, health
                    statistics, and crime rates. With this, the general public is able to plan for future decisions,
                    such as whether to bring an umbrella or whether if a city is a safe place to live.</p>

                <h3>Authors</h3>
                <div className="row panel-default">
                    <div className="card-body col border border-dark rounded">
                        <h5 className="card-title">Kevin Nguyen</h5>
                        <b>Full-Stack Engineer</b>
                        <p className="card-text">
                            <img className="img-thumbnail" alt="kevin" src={require("./images/kevin.jpg")}/><br/>
                            <p>A senior at UT majoring in Computer Science that is interested in networks, and
                                cybersecurity. Eventually is going to travel the world and the seven seas</p>
                            <img
                                src={require("./images/commits.svg")}/><b>: {this.state.result && this.state.result[0].commitsMade}</b><br/>
                            <img
                                src={require("./images/issues.svg")}/><b>: {this.state.result && this.state.result[0].issuesClosed}</b><br/>
                            <img
                                src={require("./images/unitTests.svg")}/><b>: {this.state.result && this.state.result[0].unitTests}</b><br/>
                        </p>
                    </div>
                    <div className="card-body col border border-dark rounded">
                        <h5 className="card-title">Andy Wang</h5>
                        <b>Front-End Engineer</b>
                        <p className="card-text">
                            <img className="img-thumbnail" alt="andy" src={require("./images/andy.jpg")}/><br/>
                            <p>A senior at UT majoring in Computer Science that is interested in machine learning and
                                data science. Hits the volleyball and grinds on the skateboard</p>
                            <img
                                src={require("./images/commits.svg")}/><b>: {this.state.result && this.state.result[2].commitsMade}</b><br/>
                            <img
                                src={require("./images/issues.svg")}/><b>: {this.state.result && this.state.result[2].issuesClosed}</b><br/>
                            <img
                                src={require("./images/unitTests.svg")}/><b>: {this.state.result && this.state.result[2].unitTests}</b><br/>
                        </p>
                    </div>
                    <div className="card-body col border border-dark rounded">
                        <h5 className="card-title">Vincent Li</h5>
                        <b>DevOps Engineer</b>
                        <p className="card-text">
                            <img className="img-thumbnail" alt="vincent" src={require("./images/vincent.jpg")}/><br/>
                            <p>A senior at UT majoring in Computer Science that is interested in game development and
                                back-end development. Shoots some Kobe's on the daily </p>
                            <img
                                src={require("./images/commits.svg")}/><b>: {this.state.result && this.state.result[1].commitsMade}</b><br/>
                            <img
                                src={require("./images/issues.svg")}/><b>: {this.state.result && this.state.result[1].issuesClosed}</b><br/>
                            <img
                                src={require("./images/unitTests.svg")}/><b>: {this.state.result && this.state.result[1].unitTests}</b><br/>
                        </p>
                    </div>
                    <div className="card-body col border border-dark rounded">
                        <h5 className="card-title">Preston Brown</h5>
                        <b>Full-Stack Engineer</b>
                        <p className="card-text">
                            <img className="img-thumbnail" alt="preston" src={require("./images/preston.jpg")}/><br/>
                            <p>A senior at UT majoring in Computer Science that is interested in machine learning, and
                                artificial intelligence. Lives in the world of politics day in and day out</p>
                            <img
                                src={require("./images/commits.svg")}/><b>: {this.state.result && this.state.result[4].commitsMade}</b><br/>
                            <img
                                src={require("./images/issues.svg")}/><b>: {this.state.result && this.state.result[4].issuesClosed}</b><br/>
                            <img
                                src={require("./images/unitTests.svg")}/><b>: {this.state.result && this.state.result[4].unitTests}</b><br/>
                        </p>
                    </div>
                    <div className="card-body col border border-dark rounded">
                        <h5 className="card-title">Vy Phan</h5>
                        <b>Full-Stack Engineer</b>
                        <p className="card-text">
                            <img className="img-thumbnail" alt="vy" src={require("./images/vy.jpg")}/><br/>
                            <p>A senior at UT majoring in Computer Science that is interested in full-stack development
                                and statistics. Pumps iron into his liquid cpu cooler to work hard. </p>
                            <img
                                src={require("./images/commits.svg")}/><b>: {this.state.result && this.state.result[3].commitsMade}</b><br/>
                            <img
                                src={require("./images/issues.svg")}/><b>: {this.state.result && this.state.result[3].issuesClosed}</b><br/>
                            <img
                                src={require("./images/unitTests.svg")}/><b>: {this.state.result && this.state.result[3].unitTests}</b><br/>
                        </p>
                    </div>
                </div>
                <br/>
                <h3>Statistics</h3>
                <p>Total number of
                    commits: {this.state.result && (this.state.result[0].commitsMade + this.state.result[1].commitsMade + this.state.result[2].commitsMade + this.state.result[3].commitsMade + this.state.result[4].commitsMade)}</p>
                <p>Total number of
                    issues: {this.state.result && (this.state.result[0].issuesClosed + this.state.result[1].issuesClosed + this.state.result[2].issuesClosed + this.state.result[3].issuesClosed + this.state.result[4].issuesClosed)}</p>
                <p>Total number of unit tests: 0</p>

                <h3>Data Sources</h3>
                <div className="container">

                    <div className="row">
                        <div className="col-md-3">
                            <h2>Dark Sky</h2>
                            <a href={"https://darksky.net/dev/docs"} role={"button"}>
                                <img className="tools-img"
                                     src={require("./images/darksky.png")}/>
                            </a>
                        </div>

                        <div className="col-md-3">
                            <h2>City Health</h2>
                            <a href={"https://api.cityhealthdashboard.com/"} role={"button"}>
                                <img className="tools-img"
                                     src={require("./images/cityhealth.png")}/>
                            </a>
                        </div>

                        <div className="col-md-3">
                            <h2>Crime-O-Meter</h2>
                            <a href={"https://www.crimeometer.com/docs"} role={"button"}>
                                <img className="tools-img"
                                     src={require("./images/crimeometer.jpg")}/>
                            </a>
                        </div>

                        <div className="col-md-3">
                            <h5>The United States Census Bureau</h5>
                            <a href={"https://www.census.gov/data/developers/guidance/api-user-guide.Query_Examples.html"}>
                                <img className="tools-img"
                                     src={require("./images/census.jpg")}/>
                            </a>
                        </div>

                    </div>

                </div>

                <br/>
                <br/>

                <h2>Tools Used</h2>
                <div className="container">

                    <div className="row">
                        <div className="col-md-3">
                            <h2>AWS</h2>
                            <a href="https://aws.amazon.com/" role="button">
                                <img className="tools-img"
                                     src={require("./images/aws.png")}/>
                            </a>
                        </div>

                        <div className="col-md-3">
                            <h2>Node.js</h2>
                            <a href="https://nodejs.org/en/" role="button">
                                <img className="tools-img" src={require("./images/nodejs.svg")}/>
                            </a>
                        </div>

                        <div className="col-md-3">
                            <h2>React</h2>
                            <a href="https://reactjs.org/" role="button">
                                <img className="tools-img"
                                     src={require("./images/react.png")}/>
                            </a>
                        </div>

                        <div className="col-md-3">
                            <h2>Flask</h2>
                            <a href="https://flask.palletsprojects.com/en/1.1.x/" role="button">
                                <img className="tools-img"
                                     src={require("./images/flask.png")}/>
                            </a>
                        </div>

                    </div>

                    <div className="row">

                        <div className="col-md-3">
                            <h2>Postman</h2>
                            <a href="https://www.postman.com/" role="button">
                                <img className="tools-img"
                                     src={require("./images/postman.png")}/>
                            </a>
                        </div>

                        <div className="col-md-3">
                            <h2>Jest</h2>
                            <a href="https://aws.amazon.com/" role="button">
                                <img className="tools-img"
                                     src={require("./images/jest.png")}/>
                            </a>
                        </div>

                        <div className="col-md-3">
                            <h2>PostgreSQL</h2>
                            <a href="https://www.postgresql.org/" role="button">
                                <img className="tools-img"
                                     src={require("./images/postgresql.png")}/>
                            </a>
                        </div>

                        <div className="col-md-3">
                            <h2>GoDaddy</h2>
                            <a href="https://www.godaddy.com/" role="button">
                                <img className="tools-img"
                                     src={require("./images/godaddy.png")}/>
                            </a>
                        </div>

                    </div>

                    <div className={"row"}>


                        <div className="col-md-3">
                            <h2>GitLab</h2>
                            <a href="https://gitlab.com/" role="button">
                                <img className="tools-img"
                                     src={require("./images/gitlab.png")}/>
                            </a>
                        </div>

                        <div className="col-md-3">
                            <h2>Python</h2>
                            <a href="https://www.python.org/" role="button">
                                <img className="tools-img"
                                     src={require("./images/python.png")}/>
                            </a>
                        </div>

                        <div className="col-md-3">
                            <h4>Typescript</h4>
                            <a href="https://www.typescriptlang.org/" role="button">
                                <img className="tools-img"
                                     src={require("./images/typescript.png")}/>
                            </a>
                        </div>

                        <div className="col-md-3">
                            <h4>Selenium</h4>
                            <a href="https://www.selenium.dev/" role="button">
                                <img className="tools-img"
                                     src={require("./images/selenium.jpg")}/>
                            </a>
                        </div>

                    </div>

                </div>

                <div className="row">

                    <div className="col-md-12">
                        <p>
                            <b>Docker</b>: Sets up pipelines, unit tests, and allow easy coding environment set up.<br/>
                            <b>Postman</b>: Creates the collections and API calls for our DangerZone API. Also,
                            generates the API Postman documentation.<br/>
                            <b>React</b>: Used by frontend for graphical user interface design.<br/>
                            <b>Node.js</b>: Allows us to use javascript languages to be rendered outside of browser
                            environment.<br/>
                            <b>Flask</b>: Used to develop our API calls.<br/>
                            <b>Jest</b>: Creates our unit tests.<br/>
                            <b>Selenium</b>: Creates our frontend acceptance tests.<br/>
                            <b>GoDaddy</b>: Obtained our domain name from this provider.<br/>
                            <b>GitLab</b>: Versioning of code and allows our customers to create user story issues as
                            feedback on our website. Also provided statistics for our About page.<br/>
                            <b>Python</b>: Scripts obtain data from our data sources and to populate our database.<br/>
                            <b>TypeScript</b>: Displays the text and graphical designs in our website.<br/>
                        </p>
                    </div>

                </div>

                <h2>Links</h2>
                <a href={"https://gitlab.com/ktn1234/DangerZone"}>GitLab</a>
                <br></br>
                <a href={"https://dangerzone.postman.co/collections/10517580-79429c0f-fdb5-4046-a223-5e3a4f06ea0d?version" +
                "=latest&workspace=6638a7e8-df4f-4653-8748-8f7d81e04beb"}>Postman
                    API</a>

            </div>
        );
    }
}

export default About;
