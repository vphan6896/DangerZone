import React, { Component } from 'react'
import './Visualizations.css'
// @ts-ignore
import USAMap from 'react-usa-map'
import {
    Link
} from "react-router-dom";
import ReactTooltip from "react-tooltip";
import Container from "react-bootstrap/Container";
import Chart from './Chart'
import Row from "react-bootstrap/Row";
let state_tot_crimes = require("./states_total_crimes.json");

class Visualizations extends Component {
    constructor(props: any) {
        super(props);
        this.mapHandler = this.mapHandler.bind(this)
    }

    mapHandler = (event: { target: { dataset: { name: any; }; }; }) => {
        alert(event.target.dataset.name);
    };

    statesCustomConfig = () => {
        let composeStates: any = {}
        for (let state in state_tot_crimes) {
            let tot_crimes = state_tot_crimes[state]
            composeStates[state] = {
                total_crimes: tot_crimes,
                title: tot_crimes,
                clickHandler: (event: { target: { dataset: any; }; }) => alert(tot_crimes + " crimes"),
            }
        }

        return composeStates;
    };

    render() {
        return (
            <div>
                <Container className="justify-content-center">
                    <Row>
                        <h1>USA Map of Crimes</h1>
                    </Row>
                    <Row>
                        <USAMap customize={this.statesCustomConfig()} defaultFill={"#DC143C"} onClick={this.mapHandler}  />
                    </Row>
                    <br/><hr/>
                <Chart></Chart>
            </Container></div>

        );
    }
}

export default Visualizations