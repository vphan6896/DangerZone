import React, {Component} from 'react'
import {Route, Switch} from "react-router-dom";
import './Cities.css'
import Pagination from 'react-bootstrap/Pagination'
import {Button} from "react-bootstrap";

let city_images = require('../../assets/cities/city_images.json');

class CitiesInstance extends Component {
    constructor(props) {
        super(props);
        this.state = {
            deck: this.props.deck
        }

        this.checkForDeck = this.checkForDeck.bind(this)

    }

    componentDidMount() {
        console.log("instance mounted")
        fetch("https://api.dangerzone.life/cities?city=" + (this.props.match.params).name)
            .then(res => res.json())
            .then(
                (result) => {
                    console.log("mounted instance: got the result")
                    this.setState({
                        deck: result
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    console.log("error is " + error)
                    this.setState({

                    });
                }
            )
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log("instance mounted")
    }

    checkForDeck() {
        this.setState(prevState => {
                return {
                    deck: this.props.deck
                }
            }
        )
    }

    render() {
        console.log("city instance deck length is: " + this.state.deck.length)

        let item = (this.props.match.params).name;
        console.log("city instance: item is " + item)
        let data = null
        if(this.state.deck.length > 0) {
            //iterate to correct city to display
            console.log("len inside is " +  this.state.deck.length)
            for(let i = 0; i < 9; i ++) {
                if (this.state.deck[i]["city"] === item)
                {
                    data = this.state.deck[i]
                    break;
                }
            }
        }

        console.log("city instance: data is " + data)
        return (
            <div className={"d-flex justify-content-md-center"}>
                <div>
                    <h3>{item.toString().toUpperCase()}</h3>
                    <p>State: {data && data['state'].charAt(0).toUpperCase() + data['state'].slice(1)}</p>
                    <p>Population: {data && data['population']}</p>
                    <p>Timezone: {data && data['time_zone']}</p>
                    <p>Latitude: {data && data['latitude']} </p>
                    <p>Longitude: {data && data['longitude']} </p>
                    <p>Average High: {data && (data['average_high'] === -1 ? "No Data" : data['average_high'])} </p>
                    <p>Average Low: {data && (data['average_low'] === -1 ? "No Data" : data['average_low'])} </p>
                    <p><a href={'/crime/' + item}>Total Crime Incidents:</a> No Data</p>
                    <p><a href={'/health/' + item}>Life Expectancy Index:</a> {data && (data['life_expectancy'] === -1 ? "No Data" : data['life_expectancy'])} </p>
                    <p><a href={'/climate/' + item}>Average Ozone:</a> {data && (data['average_ozone' ]=== -1 ? "No Data" : data['average_ozone'])} </p>
                </div>
            </div>
        )
    }
}

class Cities extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentPage: 0,
            citiesDeck: [],
            sort: "true"
        }

        console.log("constructor: state is " + this.state.currentPage)
        console.log("constructor: citiesDeck is " + this.state.citiesDeck)

        this.chooseHand = this.chooseHand.bind(this)
    }

    sortChange(ascending) {

        let newSort = ""
        if(ascending === "true") {
            newSort = "true"
        } else {
            newSort = "false"
        }

        fetch("https://api.dangerzone.life/cities?offset=0" + "&limit=9&column=population&asc=" + ascending)
            .then(res => res.json())
            .then(
                result => {
                    this.setState({
                        currentPage: 0,
                        citiesDeck: result,
                        sort: newSort
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                error => {
                    console.log("error is " + error)
                    this.setState({

                    });
                }
            )
    }

    componentDidMount() {
        console.log("component did mount")
        console.log("mount:page is " + this.state.currentPage)
        this.chooseHand(this.state.currentPage)
        console.log("finish mounting")
    }

    chooseHand(pageNum) {
        console.log("you clicked " + pageNum)
        let finalPage = pageNum
        if (finalPage < 0) {
            finalPage = 0
        }

        //Only 100 instances, 9 displayed, so 12 pages total
        if (finalPage > 10) {
            finalPage = 10
        }

        const itemsToDisplay = 9
        let offset = finalPage * itemsToDisplay

        fetch("https://api.dangerzone.life/cities?offset=" + offset + "&limit=9&column=population&asc=" + this.state.sort)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        currentPage: finalPage,
                        citiesDeck: result
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    console.log("error is " + error)
                    this.setState({

                    });
                }
            )

        this.setState(prevState => {
                return {
                    currentPage: finalPage
                }
            }
        )
    }

    render() {
        let itemsToDisplay = 9
        if (this.state.citiesDeck.length === 0) {
            itemsToDisplay = 0
        }

        let sample_cities = new Array(itemsToDisplay)
        console.log("render: cities deck length is " + this.state.citiesDeck.length)
        console.log("render: citiesDeck is " + this.state.citiesDeck)


        for (let i = 0; i < itemsToDisplay; i++) {
            //let offset = i + this.state.currentPage * itemsToDisplay
            //Don't read undefineds in all_data
            //if (all_data[offset]) {
            //    sample_cities[i] = all_data[offset]["city"]
            //}
            if (this.state.citiesDeck) {
                sample_cities[i] = this.state.citiesDeck[i]['city']
            }

        }

        //Populate random images for cities without image
        for (let i = 0; i < itemsToDisplay; i++) {
            if ((typeof(city_images[sample_cities[i]]) === "undefined") || (city_images[sample_cities[i]] === "")) {
                city_images[sample_cities[i]] = ("https://placeimg.com/640/480/arch?t=" + Math.random() * Math.pow(10,9))
            }
        }

        //Create unique keys for map
        let id = 0;
        console.log("sample cities are " + sample_cities)
        const citiesClickables = sample_cities.map((city) => {
            const cityInfo = this.state.citiesDeck[id];
            const population = cityInfo.population;
            const average_ozone = cityInfo.average_ozone;
            const lifeExpectancy = cityInfo.life_expectancy;

            return (<div key={id++} className="col-md-3 m-3">
                <div className="d-flex justify-content-center">
                    <h2>{String(city).toUpperCase()}</h2>
                </div>
                <a href={"/cities/" + city} role="button">
                    <img className="city-img" src={(city_images[city])} height={"100"}/>
                </a>
                <div className="d-flex justify-content-center">
                    <b>Population: {population === -1 ? "No Data" : population}</b>
                </div>
                <div className="d-flex justify-content-center">
                    <b>Average Ozone: {average_ozone === -1 ? "No Data" : average_ozone}</b>
                </div>
                <div className="d-flex justify-content-center">
                    <b>Life Expectancy: {lifeExpectancy === -1 ? "No Data" : lifeExpectancy}</b>
                </div>
            </div>
        );
    });

        return (
            <div className={"container"}>
                <Switch>
                    <Route path="/cities/:name" render={
                        (props) =>
                            <CitiesInstance {...props} deck={this.state.citiesDeck} />
                    }/>
                    <Route path="/cities" exact render={() => {
                        return (
                            <div>
                                <Button variant="outline-danger">Sort by Population </Button>
                                <Button variant="danger" active={this.state.sort === "true"}
                                        onClick={() => this.sortChange("true")}>Ascending</Button>
                                <Button variant="danger" active={this.state.sort === "false"}
                                        onClick={() => this.sortChange("false")}>Descending</Button>
                                <div className="row d-flex justify-content-center">
                                    {citiesClickables}
                                </div>
                                <Pagination className={"justify-content-center"}>
                                    <Pagination.First onClick={() => this.chooseHand(0)}/>
                                    <Pagination.Prev onClick={() => this.chooseHand(this.state.currentPage-1)}/>
                                    <Pagination.Item active={true} onClick={() => this.chooseHand(this.state.currentPage-1)}>{this.state.currentPage}</Pagination.Item>
                                    <Pagination.Next onClick={() => this.chooseHand(this.state.currentPage+1)}/>
                                    <Pagination.Last onClick={() => this.chooseHand(10)}/>
                                </Pagination>
                            </div>
                        )
                    }}/>
                </Switch>
            </div>
        );
    }
}

export default Cities