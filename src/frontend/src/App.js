import React, {Component} from 'react'
import Routes from "./components/routes/Routes";
import Navigation from "./components/navigation/Navigation"

class App extends Component {
    render() {
        return (
            <div>
                <Navigation/>
                <Routes/>
            </div>
        );
    }
}

export default App

