# Danger Zone: Technical Report
# https://www.dangerzone.life

## Motivation

Want to find the best city? Curious how a city stands compared to others? Danger Zone displays useful information to answer these questions, as well as give insights what specifically could be wrong with a city. Many aspects of ongoings in cities are provided: climate information, health statistics, and crime rates. With this, the general public are able to plan for future decisions, such as whether to bring an umbrella or whether if a city is a safe place to live.

## User Stories To Our Developers

#### ***IDB1***

#### 1) Make Website Live
Website should have custom domain name. The name should be formal and not complicated. Website should be accessible by the public through said domain name.

Estimated Time: Not Given

#### 2) Make an about page
Use GitLab API to get the number of commits and issues. Pictures of website authors should be included along with author descriptions. The description of the purpose of the website should be clear with an obvious goal in mind.

Estimated Time: Not Given

#### 3) Create 3 Instances for the Models
The proposed models City, School, and Companies should have 9 instances each visible without scrolling along with at least 5 attributes. The models should also include graphics such as images, maps, social networks, etc. Instances between models should have similar formatting if possible.

Estimated Time: Not Given

#### 4) Adding Images
Have an image of schools and/or companies to emanate the purpose of your website. They can also be used to increase the aesthetics of the website to make it more appealing. It can also be used in the navbar such as for a home icon.

Estimated Time: Not Given

#### 5) For Schools' Model
The model for schools should have plenty of searching functionality. Filtering by states or sorting by cost would be convenient. Each instance could also be linked to the school's website if possible.

Estimated Time: Not Given

#### ***IDB2***

#### 6) Implement pagination of data
This issue is after you include more data into your models. Nonetheless, the number of pages should reflect the number of instances of the model. There shouldn't be pages that don't lead anywhere are that don't have any data. At the moment with your 3 instances, just having a 'Page 1' would be appropriate.

Estimated Time: Not Given

#### 7) Create a dynamic website
Dynamic website should be hosted on AWS or GCP. Data should be have the capability of being updated when new data is inserted into the database. Number of pages of data in a model should be dynamic and reflect the amount of data there are.

Estimated Time: Not Given

#### 8) Include many instances
Models should have many instances. This would allow customers to get a better idea of what the true statistics are. Without much data to go off of, customers won't be able to reach their own choice of where to go to get a job.

Estimated Time: Not Given

#### 9) Rounding Statistics
Some of the numbers should be rounded. It would look much neater than 5 zeros after a decimal. One example is the admission rate on the Universities page. Some numbers are like '83.85000000000001' when it would look better to as 83.9 or 83.85. Personally, I think 1 decimal place would be better, but that can be up to you.

Estimated Time: Not Given

#### 10) Home page aesthetic
The home page looks nice with the slideshow. The text and button below the slideshow should be centered though. I think it would look better instead of being lopsided. Also, the text and arrows within the slideshow are pretty difficult to see. I didn't notice the 3 bars at the bottom of the slideshow for a while and I didn't notice the arrows on the sides at first. Perhaps having an outline or a different color would help bring it out.

Estimated Time: Not Given

#### ***IDB3***

#### 11) Update Pagination Buttons
The pagination buttons are just numbers at the moment. It looked much better as they way you had it before, though I understand there might've been some issues with that format. At the very least, it isn't immediately intuitive that the '...' after page 4 is something you can click on to get more pages, so I think something should be done about that.

Estimated Time: Not Given

#### 12) Update project statistics
This is just a small issue, but the number of unit tests under project statistics in the About page is still 0. I see that the number of unit tests per person is updated, though. This should just be a quick fix with just addition.

Estimated Time: Not Given

#### 13) Implement sorting, filtering, and searching
Since you seem to be mostly done with your website, this seems like the last main part of the project. It would be great if I could sort and filter on any of the available attributes. For the sorting, filtering, and searching fields, I believe it would be best to have drop down/search fields grouped at the top, but that's up to you.

Estimated Time: Not Given

#### 14) Add info about city indices
In the cities page, there are a lot of index information, such as crime index, traffic index, etc. It's not intuitive as to what the numbers mean, so we don't know if it's bad or good. If you could provide a description about the index criteria, that would help users understand better.

Estimated Time: Not Given

#### 15) Small changes
Since your website is really good, I don't anything other than some small nitpickings. Adding a '$' to tuition under university attributes would look nice. Also, graduation rate is missing a '%' while admission rate has it. For glassdoor rankings of companies, including how many stars the rating is out of would help.

Estimated Time: Not Given

## User Stories From Our Customers

#### ***IDB1***

#### 1) About page integrated with GitLab API:
As per the project requirements, the about page should contain statistics about the GitLab repository. This is done through integrating the website with GitLab API to retrieve information such as number of commits and issues.

Estimated Time: 3 Hours

Actual Time: 5 Hours

How: Called GitLab API and filtered data. Counted up all the commits and other statistics and displayed the obtained information onto the About page.

#### 2) Create 3 instances of each model:
Each model should have 3 examples. They do not need to be dynamic at the moment, but the models should have data from the APIs that will be pulled from.

Estimated Time: 8 Hours

Actual Time: 7 Hours

How: Retrieved 3 instances from each of our data sources and hard coded the information onto each model page.

#### 3) Create a nice looking home page:
The home page should look nice and convey what the website is about. It should also be easy to navigate.

Estimated Time: 2 Hours

Actual Time: 2 Hours

How: Added rounded images and changed the color of the navbar to a dark grey so it's easier on the eyes.

#### 4) Promote Customers to Reporter so we can label issues:
Promote the customers from Guest to Reporter so they are able to label issues they open. This allows greater organization capabilities for the customers when they give feedback on the website.

Estimated Time: 5 Minutes

Actual Time: 5 Minutes

How: Changed roles within GitLab.

#### 5) Post a Collection on Postman for the site's API:
Post the API collection to the website for users to view. This gives the users detailed information about the API calls and endpoints available.

Estimated Time: 3 Hours

Actual Time: 4 Hours

How: Created the collections within Postman and made the documentation website public. We made the 'API' section of the navbar redirect the user to the public Postman documention website.

#### ***IDB2***

#### 6) Expand Filters for cities in API
Currently your plan only includes the ability to filter cities by state. I believe it would be very useful to be able to filter by other criteria as well, such as by average temperature and climate, by crime rate statistics such as frequency of violent crimes, and by health metrics such as level of air pollution. I believe these additions would allow the city filter to be used to help people find the cities that are right for them.

Estimated Time: 1 Hour

Actual Time: 2 Hours

How: Added 'Worst 3 Crimes by City' call. Also, added many parameters to each model that allows very customized filterings. Now, users can filter specific fields with greater/less than options.

#### 7) Add crime information to the site
From looking at your API it appears you plan to include crime statistics. However I don't see a way to look at those from the website itself. Being able to access this data directly on the site would be very useful for less tech-savy users

Estimated Time: 8 Hours

Actual Time: 9 Hours

How: Obtained data from our data source to populate our database. Then, crime page is filled with crime data through our API.

#### 8) Add some media to the model pages
Your information on the model pages looks well made, but is not currently visually appealing. It is all displayed as simple text. I believe adding some media to the pages will make it look better and be more accessible. For example, you could include images on the climate page to represent the current weather, like a sun for sunny, or storm cloud for rainy. Another useful one would be to make the numerical health data easier to compare visually, like with some sort of meter that is higher or lower and a different color for different pollution counts, or something similar if you have a better looking idea.

Estimated Time: 3 Hours

Actual Time: 3 Hours

How: Added some pictures to the pages to make it look better.

#### 9) Add links back to the cities and from cities to stats pages
Currently your site does not link together well. If I'm looking at climate and like Austin's, I'm not able to then click on Austin from the climate page to see more information about Austin. Likewise if I'm looking at Austin's page and want to see the stats behind the health index, I can't currently click on the health index to be brought to the health page. Adding these kinds of links would make the site much more navigable.

Estimated Time: 3 Hours

Actual Time: 2 Hours

How: Added links from each instance of Health, Climate, and Crime pages that goes back to the Cities page. From the Cities page, the other 3 pages are accessible through links as well.

#### 10) Expand the technical report
Your technical report is currently lacking lots of key information. For example, you list the hosting platforms used but not how they interact or what you do on each one to make your site work. In addition, while you list the previous user stories, you don't explain what your did to implement them and how successful you were at each one. Expanding this report would make it much more useful to anyone joining your team in the future.

Estimated Time: 2 Hours

Actual Time: 2 Hours

How: Added all missing user stories and related information, such as estimated time, actual time, and how it was implemented. Updated attributes for each model and included details of what the tools and host sites were used for.

#### ***IDB3***

#### 11) Fix layout of instance pages
Your instance pages are not well laid out at the moment. It appears at first to a new user that they are still on the model table, and they have to scroll significantly before they see any information for the city. Having links to all the cities while on the instance page is distracting and a determinant to the experience. Please rework this to better the user experience.

Estimated Time: 8 Hours

Actual Time: 6 Hours

How: Each instance leads to its own page which contains all the attributes for that model. Additionally, the instances on each model page have card views that displays an image and a few attributes.

#### 12) Add relevant embedded media to instance pages
The instance pages still do not include any relevant embedded media. Per the project requirements, this was to be implemented in phase 2. Adding relevant media will greatly aid in the user experience.

Estimated Time: 5 Hours

Actual Time: NA

How: NA

#### 13) Fix page bar on model pages
The page bar on your model pages is currently bugged. It does not properly display which page I am currently on and the arrow buttons fail to work. This is very confusing and hard to navigate for the end user.

Estimated Time: 2 Hours

Actual Time: 2 Hour

How: Fully implemented the code for the page bar. Decided to display only the current page with arrows that goes to the next/previous page and arrows that skips to the first/last page.

#### 14) Add more instances
Your site currently appears to only have 3 cities. Per the project requirements, it should have at least 100 by now. Without many cities to compare, the utility of the site is greatly hindered.

Estimated Time: 7 Hours

Actual Time: 6 Hours

How: Finished the API which the website calls. Everything is set up in the backend, but it is not displayed on the frontend yet.

#### 15) Implement a searching and filtering
The site would be greatly improved if I had the ability to search for a given city or to sort and filter cities by various parameters like average temperature or crime rate. This would make it much easier to find a city or to compare cities in a given metric. The end user would have a much more pleasant experience with these features.

Estimated Time: 8 Hours

Actual Time: 10 Hours

How: Added API endpoints to allow sorting from the backend. Used ElasticSearch to allow searching. Implemented filtering in the backend.

## RESTful API

The API has data that can be categorized into four sections: city, climate, health, and crime. Most API GET calls take a city as a parameter. This allows specific information to be retrieved about individual cities. How to use the API GET calls is explained in the following Postman collection documentation.

#### Postman Collection Documentation:
https://documenter.getpostman.com/view/10517580/SzKYPGyC?version=latest

## Models

#### City

- ID
- City
- State
- Population
- Timezone
- Latitude
- Longitude
- Total Crime Incidents
- Average High Temperature
- Average Low Temperature
- Life Expectancy
- Average Ozone

#### Climate

- ID
- City ID
- Average Ozone
- Spring Average Temperature
- Spring Wind Speed
- Spring Humidity
- Spring UV
- Spring Ozone
- Summer Average Temperature
- Summer Wind Speed
- Summer Humidity
- Summer UV
- Summer Ozone
- Fall Average Temperature
- Fall Wind Speed
- Fall Humidity
- Fall UV
- Fall Ozone
- Winter Average Temperature
- Winter Wind Speed
- Winter Humidity
- Winter UV
- Winter Ozone

#### Health

- ID
- City ID
- Air Pollution Particle
- Cardiovascular Disease Deaths
- Frequent Mental Distress
- Lead Exposure Risk Index
- Opiod Overdose Deaths
- Smoking
- Diabetes
- Obesity
- Life Expectancy
- Limited Access to Healthy Foods
- Walkibility

#### Crime

- ID
- City ID
- Total Crime Incidents
- Murder
- Assault
- Drugs
- Burglary
- Vandalism
- DUI
- Sex Offenses
- Theft
- Animal Cruelty
- Other Offenses

#### Model Relationships

- *Cities*: A list of cities that directs users to other the pages with relevant information in more detail.
- *Climate*: Categorized by seasons and cities. User can be directed to cities page.
- *Health*: Number of different health cases per city. User can be directed to cities page.
- *Crime*: Number of different crime cases per city. User can be directed to cities page.

## Tools

- *Docker*: Sets up pipelines, unit tests, and allow easy coding environment set up.
- *Postman*: Creates the collections and API calls for our DangerZone API. Also, generates the API Postman documentation.
- *React*: Used by frontend for graphical user interface design.
- *Node.js*: Allows us to use javascript languages to be rendered outside of browser environment.
- *Flask*: Used to develop our API calls.
- *Jest*: Creates our unit tests.
- *Selenium*: Creates our frontend acceptance tests.
- *GoDaddy*: Obtained our domain name from this provider.
- *GitLab*: Versioning of code and allows our customers to create user story issues as feedback on our website. Also provided statistics for our About page.
- *Python*: Scripts obtain data from our data sources and to populate our database.
- *TypeScript*: Displays the text and graphical designs in our website.

## Hosting

- *AWS*: Hosts our website online for the public, as well as allows our website to be dynamic.
- *EB*: Automaticly provides resources like *EC2*, *S3*, and *ELB* to host our website.

## GitLab

https://gitlab.com/ktn1234/DangerZone