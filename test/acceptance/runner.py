from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

chrome = webdriver.Remote(
          command_executor='http://hub:4444/wd/hub',
          desired_capabilities=DesiredCapabilities.CHROME)

chrome.get('frontend:3000')
print(chrome.title)

chrome.quit()
