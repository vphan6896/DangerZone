import unittest
from unittest.mock import Mock

from werkzeug.exceptions import NotFound, BadRequest

from src.backend.local_api.cities_service import CitiesService
from src.backend.local_api.climate_service import ClimateService
from src.backend.local_api.crime_service import CrimeService
from src.backend.local_api.health_service import HealthService

class TestCity(unittest.TestCase):

    @classmethod
    def setUp(self):
        self.mock_engine = Mock()
        self.mock_connection = Mock()
        self.mock_engine.connect = Mock(return_value=self.mock_connection)
        self.service = CitiesService(self.mock_engine)

    def test_get_all_cities(self):
        query_res = [create_city(1),create_city(2),create_city(3)]
        self.mock_connection.execute = Mock(return_value=[create_city_query_result(1), create_city_query_result(2), create_city_query_result(3)])
        self.assertEqual(query_res, self.service.get_all_cities())

    def test_get_city(self):
        service_res = [create_city(2)]
        query_res = [create_city_query_result(2)]
        self.mock_connection.execute = Mock(return_value=query_res)
        self.assertEqual(service_res, self.service.get_city(2))

    def test_get_city_404(self):
        query_res = None
        self.mock_connection.execute = Mock(return_value=query_res)
        self.assertRaises(NotFound, self.service.get_city, 2)

    def test_get_cities_by_attribute(self):
        service_res = [create_city(2),create_city(2)]
        query_res = [create_city_query_result(2),create_city_query_result(2)]
        self.mock_connection.execute = Mock(return_value=query_res)
        self.assertEqual(service_res, self.service.get_cities_by_attribute("population",2))

    def test_get_cities_by_attribute_400(self):
        self.mock_connection.execute = Mock(side_effect=Exception())
        self.assertRaises(BadRequest, self.service.get_cities_by_attribute, "population", 2)

def create_city(num):
    city = {
        'city': num,
        'state': num,
        'population': num,
        'time_zone': num,
        'latitude': num,
        'longitude': num,
        'average_high': num,
        'average_low': num,
        'average_ozone': num,
        'life_expectancy': num
    }
    return city

def create_city_query_result(num):
    result = []
    for x in range(0,11):
        result.append(num)
    return result


class TestClimate(unittest.TestCase):

    @classmethod
    def setUp(self):
        self.mock_engine = Mock()
        self.mock_connection = Mock()
        self.mock_engine.connect = Mock(return_value=self.mock_connection)
        self.service = ClimateService(self.mock_engine)

    def test_get_all_cities_climate(self):
        service_res = [create_climate("Austin",1),create_climate("Austin",2),create_climate("Austin",3)]
        query_res = [create_climate_query_result("Austin",1),create_climate_query_result("Austin",2),create_climate_query_result("Austin",3)]
        self.mock_connection.execute = Mock(return_value=query_res)
        self.service.get_corresponding_city = Mock(return_value="Austin")
        self.assertEqual(service_res, self.service.get_all_cities_climate())

    def test_get_climate_by_season(self):
        service_res = [create_climate_season("winter","Austin",2)]
        query_res = [create_climate_query_result_single_season("Austin",2)]
        self.mock_connection.execute = Mock(return_value=query_res)
        self.service.get_corresponding_city = Mock(return_value="Austin")
        self.assertEqual(service_res, self.service.get_climate_by_season("winter"))

    def test_get_climate_by_season_400(self):
        self.mock_connection.execute = Mock(side_effect=Exception())
        self.assertRaises(Exception, self.service.get_climate_by_season, "winter")

    def test_get_climate_by_city(self):
        service_res = [create_climate("Austin", 2)]
        query_res = [create_climate_query_result("Austin",2)]
        self.mock_connection.execute = Mock(return_value=query_res)
        self.assertEqual(service_res, self.service.get_climate_by_city("Austin"))

    def test_get_climate_by_city_400(self):
        self.mock_connection.execute = Mock(side_effect=Exception())
        self.service.get_corresponding_city = Mock(return_value="Austin")
        self.service.get_corresponding_city_id = Mock(return_value="Austin")
        self.assertRaises(BadRequest, self.service.get_climate_by_city, "Austin")

    def test_get_climate_by_city_and_season(self):
        service_res = [create_climate_season("winter","Austin", 2)]
        query_res = [create_climate_query_result("Austin", 2)]
        self.mock_connection.execute = Mock(return_value=query_res)
        self.service.get_corresponding_city_id = Mock(return_value="Austin")
        self.assertEqual(service_res, self.service.get_climate_by_city_and_season("city","season","Austin","winter"))

    def test_get_climate_by_city_and_season_400(self):
        self.mock_connection.execute = Mock(side_effect=Exception())
        self.service.get_corresponding_city_id = Mock(return_value="Austin")
        self.assertRaises(Exception, self.service.get_climate_by_city_and_season,"city","season","","")

def create_climate(city, num):
    climate = {
                'city': city,
                'average_ozone': num,
                'spring': {
                    "temperature": num,
                    "windSpeed": num,
                    "humidity": num,
                    "uv": num,
                    "ozone": num,
                },
                "summer": {
                    "temperature": num,
                    "windSpeed": num,
                    "humidity": num,
                    "uv": num,
                    "ozone": num,
                },
                "fall": {
                    "temperature": num,
                    "windSpeed": num,
                    "humidity": num,
                    "uv": num,
                    "ozone": num,
                },
                "winter": {
                    "temperature": num,
                    "windSpeed": num,
                    "humidity": num,
                    "uv":num,
                    "ozone": num,
                },
            }
    return climate


def create_climate_season(season, city, num):
    climate = {
        'city': city,
        season: {
            "temperature": num,
            "windSpeed": num,
            "humidity": num,
            "uv":num,
            "ozone": num,
        },
    }
    return climate

def create_climate_query_result(city, num):
    result = []
    result.append(0)
    result.append(city)
    for x in range(0,22):
        result.append(num)
    return result

def create_climate_query_result_single_season(city,num):
    result = []
    result.append(city)
    for x in range(0,5):
        result.append(num)
    return result

class TestCrime(unittest.TestCase):

    @classmethod
    def setUp(self):
        self.mock_engine = Mock()
        self.mock_connection = Mock()
        self.mock_engine.connect = Mock(return_value=self.mock_connection)
        self.service = CrimeService(self.mock_engine)

    def test_get_all_cities_crimes(self):
        service_res = [create_crime("Austin",1),create_crime("Austin",2),create_crime("Austin",3)]
        query_res = [create_crime_query_result(1),create_crime_query_result(2),create_crime_query_result(3)]
        self.mock_connection.execute = Mock(return_value=query_res)
        self.service.get_corresponding_city = Mock(return_value="Austin")
        self.assertEqual(service_res, self.service.get_all_cities_crimes())

    def test_get_city_crime(self):
        service_res = [create_crime("Austin", 1)]
        query_res = [create_crime_query_result(1)]
        self.mock_connection.execute = Mock(return_value=query_res)
        self.service.get_corresponding_city_id = Mock(return_value="Austin")
        self.service.get_corresponding_city = Mock(return_value="Austin")
        self.assertEqual(service_res, self.service.get_city_crime("Austin"))

    def test_get_city_crime_400(self):
        self.mock_connection.execute = Mock(side_effect=Exception())
        self.service.get_corresponding_city_id = Mock(return_value="Austin")
        self.service.get_corresponding_city = Mock(return_value="Austin")
        self.assertRaises(Exception, self.service.get_city_crime,"Austin")

    def test_get_worst_three_city_crimes(self):
        service_res = [{
                'city': "Austin",
                'murder': 5,
                'assault': 4,
                'drugs': 3
            }]
        query_res = [[0,0,0,5,4,3,0,0,0,0,0,0]]
        self.mock_connection.execute = Mock(return_value=query_res)
        self.service.get_corresponding_city_id = Mock(return_value="Austin")
        self.service.get_corresponding_city = Mock(return_value="Austin")
        self.assertEqual(service_res, self.service.get_worst_three_city_crimes("Austin"))

    def test_get_city_crime_by_filter(self):
        service_res = [create_crime("Austin", 1)]
        query_res = [create_crime_query_result(1)]
        self.mock_connection.execute = Mock(return_value=query_res)
        self.service.get_corresponding_city_id = Mock(return_value="Austin")
        self.service.get_corresponding_city = Mock(return_value="Austin")
        self.assertEqual(service_res, self.service.get_city_crime_by_filter("murder_lt",5))

    def test_get_city_crime_by_filter_400(self):
        self.mock_connection.execute = Mock(side_effect=Exception())
        self.service.get_corresponding_city_id = Mock(return_value="Austin")
        self.service.get_corresponding_city = Mock(return_value="Austin")
        self.assertRaises(Exception, self.service.get_city_crime_by_filter,"murder_lt",5)

def create_crime(city,num):
    crime = {
        'city': city,
        'total_crime_incidents': num,
        'murder': num,
        'assault': num,
        'drugs': num,
        'burglary': num,
        'vandalism': num,
        'dui': num,
        'sex_offenses': num,
        'theft': num,
        'animal_cruelty': num,
        'other_offenses': num
    }
    return crime

def create_crime_query_result(num):
    result = []
    for x in range(0, 13):
        result.append(num)
    return result

class TestHealth(unittest.TestCase):

    @classmethod
    def setUp(self):
        self.mock_engine = Mock()
        self.mock_connection = Mock()
        self.mock_engine.connect = Mock(return_value=self.mock_connection)
        self.service = HealthService(self.mock_engine)

    def test_get_all_cities_health(self):
        service_res = [create_health("Austin",1),create_health("Austin",2),create_health("Austin",3)]
        query_res = [create_health_query_result(1),create_health_query_result(2),create_health_query_result(3)]
        self.mock_connection.execute = Mock(return_value=query_res)
        self.service.get_corresponding_city = Mock(return_value="Austin")
        self.assertEqual(service_res, self.service.get_all_cities_health())

    def test_get_city_health(self):
        service_res = [create_health("Austin", 1)]
        query_res = [create_health_query_result(1)]
        self.mock_connection.execute = Mock(return_value=query_res)
        self.service.get_corresponding_city_id = Mock(return_value="Austin")
        self.service.get_corresponding_city = Mock(return_value="Austin")
        self.assertEqual(service_res, self.service.get_health_by_city("Austin"))

    def test_get_city_health_400(self):
        self.mock_connection.execute = Mock(side_effect=Exception())
        self.service.get_corresponding_city_id = Mock(return_value="Austin")
        self.service.get_corresponding_city = Mock(return_value="Austin")
        self.assertRaises(Exception, self.service.get_health_by_city,"Austin")

    def test_get_city_health_by_filter(self):
        service_res = [create_health("Austin", 1)]
        query_res = [create_health_query_result(1)]
        self.mock_connection.execute = Mock(return_value=query_res)
        self.service.get_corresponding_city_id = Mock(return_value="Austin")
        self.service.get_corresponding_city = Mock(return_value="Austin")
        self.assertEqual(service_res, self.service.get_city_health_by_filter("obesity_lt",5))

    def test_get_city_health_by_filter_400(self):
        self.mock_connection.execute = Mock(side_effect=Exception())
        self.service.get_corresponding_city_id = Mock(return_value="Austin")
        self.service.get_corresponding_city = Mock(return_value="Austin")
        self.assertRaises(Exception, self.service.get_city_health_by_filter,"obesity_lt",5)

def create_health(city, num):
    return {
                'city': city,
                'air_pollution_particle': num,
                'cardiovascular_disease_deaths': num,
                'frequent_mental_distress': num,
                'lead_exposure_risk_index': num,
                'opioid_overdose_deaths': num,
                'smoking': num,
                'diabetes': num,
                'obesity': num,
                'life_expectancy': num,
                'limited_access_to_healthy_foods': num,
                'walkability': num,
            }

def create_health_query_result(num):
    result = []
    for x in range(0, 13):
        result.append(num)
    return result

if __name__ == '__main__':
    unittest.main()

